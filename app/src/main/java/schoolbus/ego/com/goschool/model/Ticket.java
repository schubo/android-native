package schoolbus.ego.com.goschool.model;

import android.location.Address;

import java.util.Date;
import java.util.List;

public class Ticket {

    public static String TAG_StartAddress="StartAddress";
    public static String TAG_DestinationAddress="DestinationAddress";
    public static String TAG_StartLongitude="StartLongitude";
    public static String TAG_StartLatitude="StartLatitude";
    public static String TAG_DestLongitude="DestLongitude";
    public static String TAG_DestLatitude="DestLatitude";


    private Address startAddress;
    private Address  destinationAddress;
    private String tripType;
    private String status;
    private Date date;
    private String bookingId;
    private String name;
    private List<Child> childrenList;
    private School school;
    private MeetingPoint meetingPoint;
    private Parents parent;
    private String sourceLatitude="0.0";
    private String sourceLongitude="0.0";
    private String destLatitude="0.0";
    private String destLongitude="0.0";

    public Ticket(){

    }

    public Ticket(Address startAddress, Address destinationAddress, Date date,String bookingId,String name) {
        this.startAddress = startAddress;
        this.destinationAddress = destinationAddress;
        this.date = date;
        this.bookingId=bookingId;
        this.name=name;
    }

    public Address getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(Address startAddress) {
        this.startAddress = startAddress;
    }

    public Address getDestinationAddress() {
        return destinationAddress;
    }

    public void setDestinationAddress(Address destinationAddress) {
        this.destinationAddress = destinationAddress;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Child> getChildrenList() {
        return childrenList;
    }

    public void setChildrenList(List<Child> childrenList) {
        this.childrenList = childrenList;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public Parents getParent() {
        return parent;
    }

    public void setParent(Parents parent) {
        this.parent = parent;
    }

    public MeetingPoint getMeetingPoint() {
        return meetingPoint;
    }

    public void setMeetingPoint(MeetingPoint meetingPoint) {
        this.meetingPoint = meetingPoint;
    }

    public String getSourceLatitude() {
        return sourceLatitude;
    }

    public void setSourceLatitude(String sourceLatitude) {
        this.sourceLatitude = sourceLatitude;
    }

    public String getSourceLongitude() {
        return sourceLongitude;
    }

    public void setSourceLongitude(String sourceLongitude) {
        this.sourceLongitude = sourceLongitude;
    }

    public String getDestLatitude() {
        return destLatitude;
    }

    public void setDestLatitude(String destLatitude) {
        this.destLatitude = destLatitude;
    }

    public String getDestLongitude() {
        return destLongitude;
    }

    public void setDestLongitude(String destLongitude) {
        this.destLongitude = destLongitude;
    }
}
