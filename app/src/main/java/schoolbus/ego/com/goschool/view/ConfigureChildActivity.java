package schoolbus.ego.com.goschool.view;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.APIcallManager;
import schoolbus.ego.com.goschool.manager.SharedPreferenceManager;
import schoolbus.ego.com.goschool.manager.ViewChildAdapter;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.School;
import schoolbus.ego.com.goschool.utils.Helper;
import schoolbus.ego.com.goschool.view.customView.ChildRegistrationForm;
import schoolbus.ego.com.goschool.view.customView.RecyclerItemClickListener;

public class ConfigureChildActivity extends AppCompatActivity implements View.OnClickListener,APIcallManager.ChildrenApiStatus,APIcallManager.DeleteChildApiStatus {

    private final String TAG=this.getClass().getSimpleName();

   private  View loadLayout,errorLayout,contentLayout;

    private RecyclerView mRecyclerView;
    private ViewChildAdapter mAdapter;
    private FloatingActionButton fab;
    private TextView tvErrorMsg;
    private MenuItem refresh;
    private List<Child> list=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure_child);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(R.string.config_child);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        loadLayout=findViewById(R.id.child_loading);
        errorLayout=findViewById(R.id.child_error);
        tvErrorMsg=errorLayout.findViewById(R.id.tv_error);
        contentLayout=findViewById(R.id.child_content);
        fab=findViewById(R.id.child_fab);
        CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();

        fab.setOnClickListener(this);


        // init view
        errorLayout.setVisibility(View.INVISIBLE);
        loadLayout.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.INVISIBLE);

        p.setAnchorId(View.NO_ID);
        fab.setLayoutParams(p);
        fab.setVisibility(View.GONE);

        // make request
        SharedPreferenceManager sMgr=new SharedPreferenceManager();
       if( sMgr.retrieveParentJWT(this)!=null|sMgr.retrieveParentJWT(this).equals("")){
           APIcallManager apiMgr=new APIcallManager();
           apiMgr.getAllChildren(this,sMgr.retrieveParentJWT(this),this);
       }else{
           tvErrorMsg.setText("Not Signed In");
           errorLayout.setVisibility(View.VISIBLE);
           loadLayout.setVisibility(View.INVISIBLE);
           contentLayout.setVisibility(View.INVISIBLE);
           p.setAnchorId(View.NO_ID);
           fab.setLayoutParams(p);
           fab.setVisibility(View.GONE);
       }


//getting the recyclerview from xml
        mRecyclerView = (RecyclerView) findViewById(R.id.registered_child_recycler_view);
        //mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));


       /* mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        Child obj=list.get(position);
                        School school=list.get(position).getSchool();
                        Intent intent=new Intent(ConfigureChildActivity.this,ViewRegisteredChild.class);
                       intent.putExtra(Child.TAG_NAME,obj.getName());
                        intent.putExtra(Child.TAG_ID,obj.getId());
                        intent.putExtra(Child.TAG_ACTIVE,obj.isActive());
                        intent.putExtra(Child.TAG_SCHOOL,school.getName());
                        intent.putExtra(Child.TAG_IMAGE, obj.getAvatarURL());
                        startActivity(intent);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );*/

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_child_configuration, menu);
       refresh = menu.findItem(R.id.action_refresh);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }if(item.getItemId()== R.id.action_refresh){
            onRestart();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Intent i = new Intent(ConfigureChildActivity.this, ConfigureChildActivity.class);  //your class
        startActivity(i);
        finish();
    }

    @Override
    public void onClick(View v) {
       switch (v.getId()){
            case R.id.child_fab:
                SharedPreferenceManager sMgr=new SharedPreferenceManager();
                if( sMgr.retrieveParentJWT(this)!=null|sMgr.retrieveParentJWT(this).equals("")) {
                    APIcallManager apiMgr = new APIcallManager();

                    apiMgr.getSchoolList(this,sMgr.retrieveParentJWT(this), schoolListListener);
                }

                // make request
               /* SharedPreferenceManager sMgr=new SharedPreferenceManager();
                if( sMgr.retrieveParentJWT(this)!=null|sMgr.retrieveParentJWT(this).equals("")) {
                    APIcallManager apiMgr = new APIcallManager();
                    apiMgr.createChild(this, sMgr.retrieveParentJWT(this));

                    break;
                }*/
        }
    }

    @Override
    public void onSucess(List<Child> list) {
        //set adapter to recyclerview
        this.list=list;
        if(list.size()>0){
            mAdapter = new ViewChildAdapter(list,this);
            mRecyclerView.setAdapter(mAdapter);
            // init view
            errorLayout.setVisibility(View.INVISIBLE);
            loadLayout.setVisibility(View.INVISIBLE);
            contentLayout.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        }else {
            tvErrorMsg.setText("No Child Configured");
            errorLayout.setVisibility(View.VISIBLE);
            loadLayout.setVisibility(View.INVISIBLE);
            contentLayout.setVisibility(View.INVISIBLE);
            fab.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void onFailure(String msg) {
        // init view
        tvErrorMsg.setText(msg);
        errorLayout.setVisibility(View.VISIBLE);
        loadLayout.setVisibility(View.INVISIBLE);
        contentLayout.setVisibility(View.INVISIBLE);
        fab.setVisibility(View.VISIBLE);
    }
    APIcallManager.SchoolApiStatus schoolListListener =new APIcallManager.SchoolApiStatus() {
        @Override
        public void onSuccess(List<School> list) {
            Log.d("tag","onSuccess");
            startActivity(new Intent(ConfigureChildActivity.this,ChildRegistrationForm.class));
            finish();
        }

        @Override
        public void onFailure(String msg) {
            Log.d("tag","onFailure");
            Helper.showErrorDialog(ConfigureChildActivity.this,"Could not load form, Try later!");
        }
    };


    @Override
    public void onDeleteSuccess() {
       Log.d(TAG,"onDeleteSuccess");
       recreate();
    }

    @Override
    public void onDeleteFailure(String msg) {
        Log.d(TAG,"onDeleteFailure");
    }
}
