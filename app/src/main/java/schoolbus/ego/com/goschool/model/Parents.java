package schoolbus.ego.com.goschool.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Parents implements Parcelable {
    private int id;
    private String firstName;
    private String lastName;
    private String street;
    private int streetNumber;
    private String zipcode;
    private String town;
    private String phoneNo;

    public Parents(){

    }

    protected Parents(Parcel in) {
        id = in.readInt();
        firstName = in.readString();
        lastName = in.readString();
        street = in.readString();
        streetNumber = in.readInt();
        zipcode = in.readString();
        town = in.readString();
        phoneNo = in.readString();
    }

    public static final Creator<Parents> CREATOR = new Creator<Parents>() {
        @Override
        public Parents createFromParcel(Parcel in) {
            return new Parents(in);
        }

        @Override
        public Parents[] newArray(int size) {
            return new Parents[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(street);
        dest.writeInt(streetNumber);
        dest.writeString(zipcode);
        dest.writeString(town);
        dest.writeString(phoneNo);
    }
}
