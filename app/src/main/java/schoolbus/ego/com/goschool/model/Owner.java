package schoolbus.ego.com.goschool.model;

public class Owner {
    private String type;
    private Parents parent;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Parents getParent() {
        return parent;
    }

    public void setParent(Parents parent) {
        this.parent = parent;
    }
}
