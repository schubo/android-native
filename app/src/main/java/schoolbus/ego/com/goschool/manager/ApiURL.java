package schoolbus.ego.com.goschool.manager;

public class ApiURL {
    public static final String baseURL="https://schubo.herokuapp.com/";
    //https://schubo.herokuapp.com/
    public static final String loginURL=baseURL+"api/v1/login";
    public static final String childrenURL=baseURL+"api/v1/children";
    public static final String schoolURL=baseURL+"api/v1/schools";
    public static final String createChildrenURL=baseURL+"api/v1/children";
    public static final String bookingParentTokenURL=baseURL+"api/v1/bookings";
    public static final String createBookingURL=baseURL+"api/v1/bookings";
    public static final String pairChildURL=baseURL+"api/v1/children/";
    public static final String registerDeviceURL=baseURL+"api/v1/devices";
    public static final String deleteChildURL=baseURL+"/api/v1/children/";
    public static final String deleteBookingURL=baseURL+"/api/v1/bookings/";
    public static final String meetingPointURL=baseURL+"/api/v1/meeting_points";
}
