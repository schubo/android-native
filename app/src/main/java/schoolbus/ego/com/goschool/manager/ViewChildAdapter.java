package schoolbus.ego.com.goschool.manager;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import schoolbus.ego.com.goschool.R;

import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.School;
import schoolbus.ego.com.goschool.view.ConfigureChildActivity;
import schoolbus.ego.com.goschool.view.ViewRegisteredChild;

public class ViewChildAdapter extends RecyclerView.Adapter<ViewChildAdapter.ChildViewHolder>{
    private List<Child> childItemList;
    private Context context;
   private ProgressDialog dialog;

    public ViewChildAdapter(List<Child> grocderyItemList, Context context) {
        this.childItemList = grocderyItemList;
        this.context = context;
       dialog=new ProgressDialog(context);
    }
    @Override
    public ChildViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View childView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_registered_child_card, parent, false);
        ChildViewHolder vh = new ChildViewHolder(childView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ChildViewHolder holder, final int position) {
        holder.childIcon.setImageUrl(childItemList.get(position).getAvatarURL(),VolleySingleton.getInstance(context).getImageLoader());
        holder.childName.setText(childItemList.get(position).getName());
        holder.deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.setMessage("Deleting Child...");
                dialog.show();
                SharedPreferenceManager sMgr = new SharedPreferenceManager();
                if (sMgr.retrieveParentJWT(context) != null | sMgr.retrieveParentJWT(context).equals("")) {
                    APIcallManager apiMgr = new APIcallManager();
                    apiMgr.deleteChild(context, sMgr.retrieveParentJWT(context),childItemList.get(position).getId(),apiStatus);
                } else {
                  dialog.setMessage("Could not be deleted");
                  dialog.dismiss();

                }
            }
        });
        holder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Child obj=childItemList.get(position);
                School school=childItemList.get(position).getSchool();
                Intent intent=new Intent(context,ViewRegisteredChild.class);
                intent.putExtra(Child.TAG_NAME,obj.getName());
                intent.putExtra(Child.TAG_ID,obj.getId());
                intent.putExtra(Child.TAG_ACTIVE,obj.isActive());
                intent.putExtra(Child.TAG_SCHOOL,school.getName());
                intent.putExtra(Child.TAG_IMAGE, obj.getAvatarURL());
                context.startActivity(intent);
            }
        });



    }
    APIcallManager.DeleteChildApiStatus apiStatus=new APIcallManager.DeleteChildApiStatus() {
        @Override
        public void onDeleteSuccess() {
            ((ConfigureChildActivity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(dialog!=null) {
                        dialog.setMessage("Deleted Successfully");
                        dialog.dismiss();
                        if(dialog.isShowing())
                            dialog.dismiss();
                        ((ConfigureChildActivity)context).onDeleteSuccess();
                }
            }

            });
        }

        @Override
        public void onDeleteFailure(final String msg) {
            ((ConfigureChildActivity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (dialog != null) {
                        dialog.setMessage("Deletion Failed");
                        dialog.dismiss();
                        if (dialog.isShowing())
                            dialog.dismiss();
                    }
                    ((ConfigureChildActivity) context).onDeleteFailure(msg);
                }


            });
        }
    };

    @Override
    public int getItemCount() {
        return childItemList.size();
    }
    public class ChildViewHolder extends RecyclerView.ViewHolder {
        NetworkImageView childIcon;
        TextView childName;
        ImageView deleteIcon;
        ImageView details;

        public ChildViewHolder(View view) {
            super(view);
            childIcon=view.findViewById(R.id.view_child_icon);
            childName=view.findViewById(R.id.tv_child_name);
            deleteIcon=view.findViewById(R.id.iv_delete_icon);
            details=view.findViewById(R.id.iv_details_icon);
        }
    }
}
