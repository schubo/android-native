package schoolbus.ego.com.goschool.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.APIcallManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViewById(R.id.btn_login_parent).setOnClickListener(this);
        findViewById(R.id.btn_login_rider).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login_parent:
               finish();
               startActivity(new Intent(LoginActivity.this,ParentLoginActivity.class));
                break;
            case R.id.btn_login_rider:
                finish();
                startActivity(new Intent(LoginActivity.this,ChildDashboardActivity.class));
                break;
        }
    }
}
