package schoolbus.ego.com.goschool.manager;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.MeetingPoint;
import schoolbus.ego.com.goschool.model.Owner;
import schoolbus.ego.com.goschool.model.Parents;
import schoolbus.ego.com.goschool.model.School;
import schoolbus.ego.com.goschool.model.Ticket;
import schoolbus.ego.com.goschool.model.UserModel;
import schoolbus.ego.com.goschool.utils.CustomException;
import schoolbus.ego.com.goschool.utils.Helper;

public class JsonParser {
    public List<Child> parseChildren(String input){
        List<Child> list=new ArrayList<>();
        try {
            JSONArray itemArray=new JSONArray(input);
            for (int i = 0; i < itemArray.length(); i++) {
                JSONObject obj=itemArray.getJSONObject(i);
                Child child=new Child();
                child.setId(obj.optString("id",""));
                child.setName(obj.optString("name",""));
                child.setStage(obj.optInt("stage",-1));
                child.setActive(obj.optBoolean("active",false));
                child.setAvatarURL(obj.optString("avatar_url",null));
                child.setSchool(parseSchool(obj.optString("school","")));
                child.setParents(parseListParents(obj.optString("parents")));
                list.add(child);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;

    }
    public Child parseChild(String input){
        Child child=new Child();
        try {
            JSONObject obj=new JSONObject(input);
            child.setId(obj.optString("id",""));
            child.setName(obj.optString("name",""));
            child.setStage(obj.optInt("stage",-1));
            child.setActive(obj.optBoolean("active",false));
            child.setAvatarURL(obj.optString("avatar_url",null));
            child.setSchool(parseSchool(obj.optString("school","")));
            child.setParents(parseListParents(obj.optString("parents")));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return child;

    }
    public List<School> parseSchoolList(String input){
        List<School> list=new ArrayList<>();
        try {
            JSONArray itemArray=new JSONArray(input);
            for (int i = 0; i < itemArray.length(); i++) {

                list.add(parseSchool(itemArray.getJSONObject(i).toString()));
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }
    public List<Ticket> parseBookingList(Context context, String input){
        List<Ticket> list=new ArrayList<>();
        try{
            JSONArray itemArray=new JSONArray(input);
            for (int i = 0; i < itemArray.length(); i++) {
                Ticket ticket=new Ticket();
                JSONObject obj=itemArray.getJSONObject(i);
                ticket.setBookingId(obj.optString("id",""));
                if(obj.getString("date")!=null){
                    try {
                       ticket.setDate( Helper.convertToDate(obj.getString("date")));
                    } catch (CustomException e) {
                        e.printStackTrace();
                        ticket.setDate(new Date(""));
                    }
                }else {
                    ticket.setDate(new Date(""));
                }
                ticket.setTripType(obj.optString("trip_type",""));
                ticket.setStatus(obj.optString("active",""));
                ticket.setSchool(parseSchool(obj.optString("school","")));
                ticket.setParent(parseParents(obj.optString("parent","")));
                ticket.setChildrenList(parseChildList(obj.optString("children","")));
                ticket.setMeetingPoint(parseMeetingPoint(obj.optString("meeting_point","")));

                if(ticket.getTripType().equalsIgnoreCase("meeting_point_to_school")){
                    if(ticket.getMeetingPoint()!=null){
                       ticket.setSourceLatitude(ticket.getMeetingPoint().getLatitude());
                       ticket.setSourceLongitude(ticket.getMeetingPoint().getLongitude());

                    }
                    if(ticket.getSchool()!=null){
                        ticket.setDestLatitude(ticket.getSchool().getLatitude());
                        ticket.setDestLongitude(ticket.getSchool().getLongitude());

                    }

                }else if(ticket.getTripType().equalsIgnoreCase("school_to_meeting_point")){
                    if(ticket.getSchool()!=null){
                        ticket.setSourceLatitude(ticket.getSchool().getLatitude());
                        ticket.setSourceLongitude(ticket.getSchool().getLongitude());
                    }

                    if(ticket.getMeetingPoint()!=null){
                        ticket.setDestLatitude(ticket.getMeetingPoint().getLatitude());
                        ticket.setDestLongitude(ticket.getMeetingPoint().getLongitude());
                    }


                }else if(ticket.getTripType().equalsIgnoreCase("both_ways")){
                    if(ticket.getMeetingPoint()!=null){
                        ticket.setSourceLatitude(ticket.getMeetingPoint().getLatitude());
                        ticket.setSourceLongitude(ticket.getMeetingPoint().getLongitude());
                    }
                    if(ticket.getSchool()!=null){
                        ticket.setDestLatitude(ticket.getSchool().getLatitude());
                        ticket.setDestLongitude(ticket.getSchool().getLongitude());
                    }
                }

                try {
                    ticket.setStartAddress(Helper.getAddress(context,Double.parseDouble(ticket.getSourceLatitude()),Double.parseDouble(ticket.getSourceLongitude())));
                } catch (CustomException e) {
                    e.printStackTrace();
                }
                try {
                    ticket.setDestinationAddress(Helper.getAddress(context,Double.parseDouble(ticket.getDestLatitude()),Double.parseDouble(ticket.getDestLongitude())));
                } catch (CustomException e) {
                    e.printStackTrace();
                }
                StringBuilder name=new StringBuilder();
               for(int j=0;j<ticket.getChildrenList().size();j++){
                   name.append(ticket.getChildrenList().get(j).getName());
                   name.append(",");

               }
               ticket.setName(name.toString());
                list.add(ticket);
            }
        }catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;

    }
    public List<MeetingPoint> parseMeetingPointList(String input){
        List<MeetingPoint> list=new ArrayList<>();
        try{
            JSONArray array=new JSONArray(input);
            for(int i=0;i<array.length();i++){
                list.add(parseMeetingPoint(array.getJSONObject(i).toString()));
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
       return list;
    }

    public MeetingPoint parseMeetingPoint(String input) {
        MeetingPoint point =new MeetingPoint();
        try {
            JSONObject object=new JSONObject(input);
            point.setId(object.optInt("id",0));
            point.setName(object.optString("name",""));
            point.setStreet(object.optString("street",""));
            point.setStreetNumber(object.optString("street_number",""));
            point.setZipCode(object.optString("zipcode",""));
            point.setTown(object.optString("town",""));
            point.setLatitude(object.optString("latitude",""));
            point.setLongitude(object.optString("longitude",""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return point;
    }

    private List<Child> parseChildList(String children) {
        List<Child> list=new ArrayList<>();
        try {
            JSONArray itemArray=new JSONArray(children);
            for (int i = 0; i < itemArray.length(); i++) {
                JSONObject obj=itemArray.getJSONObject(i);
                Child child=new Child();
                child.setId(obj.optString("id",""));
                child.setName(obj.optString("name",""));
                child.setStage(obj.optInt("stage",-1));
                child.setActive(obj.optBoolean("active",false));
                child.setAvatarURL(obj.optString("avatar_url",null));
                list.add(child);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    private List<Parents> parseListParents(String input){
        List<Parents> list=new ArrayList<>();
        try {
            JSONArray array=new JSONArray(input);
            for(int i=0;i<array.length();i++){
                list.add(parseParents(array.optJSONObject(i).toString()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    private Parents parseParents(String input)  {
        Parents parent=new Parents();
        try {
            JSONObject obj=new JSONObject(input);
            parent.setId(obj.optInt("id",-1));
            parent.setFirstName(obj.optString("first_name",""));
            parent.setLastName(obj.optString("last_name",""));
            parent.setStreet(obj.optString("street",""));
            parent.setStreetNumber(obj.optInt("street_number",-1));
            parent.setZipcode(obj.optString("zipcode",""));
            parent.setTown(obj.optString("town",""));
            parent.setPhoneNo(obj.optString("phone_no",""));


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  parent;
    }

    private School parseSchool(String input) {
        School school=new School();
        try {
            JSONObject obj=new JSONObject(input);
            school.setId(obj.optInt("id",-1));
            school.setName(obj.optString("name",""));
            school.setStreet(obj.optString("street",""));
            school.setStreetNumber(obj.optString("street_number",""));
            school.setZipcode(obj.optInt("zipcode",-1));
            school.setTown(obj.optString("town",""));
            school.setPhoneNumber(obj.optString("phone_no",""));
            school.setLatitude(obj.optString("latitude",""));
            school.setLongitude(obj.optString("longitude",""));
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return school;
    }
    public UserModel parseUserModel(String input){
        UserModel user=new UserModel();
        try{
         JSONObject obj=new JSONObject(input);
         user.setId(obj.optInt("id",-1));
         user.setToken(obj.optString("token",""));
         user.setOs(obj.optString("os",""));
            Owner owner=new Owner();
            JSONObject ownerObj=new JSONObject(obj.optString("owner",""));
            owner.setType(ownerObj.optString("type",""));
            owner.setParent(parseParents(ownerObj.optString("parent","")));
            user.setOwner(owner);
        }catch (Exception ex){
            ex.printStackTrace();
        }
      return user;
    }
}
