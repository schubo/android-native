package schoolbus.ego.com.goschool.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.application.SchuboApp;
import schoolbus.ego.com.goschool.manager.APIcallManager;
import schoolbus.ego.com.goschool.manager.SharedPreferenceManager;
import schoolbus.ego.com.goschool.manager.TicketAdapter;
import schoolbus.ego.com.goschool.manager.ViewChildAdapter;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.MeetingPoint;
import schoolbus.ego.com.goschool.model.School;
import schoolbus.ego.com.goschool.model.Ticket;
import schoolbus.ego.com.goschool.utils.DataProviderHelper;
import schoolbus.ego.com.goschool.utils.Helper;
import schoolbus.ego.com.goschool.view.customView.ChildRegistrationForm;
import schoolbus.ego.com.goschool.view.customView.RecyclerItemClickListener;

public class BookingActivity extends AppCompatActivity implements View.OnClickListener,APIcallManager.BookingDetailsApiStatus ,APIcallManager.DeleteBookingApiStatus{

    private View loadLayout,errorLayout,contentLayout;
    private TextView tvErrorMsg;
    private MenuItem refresh;
    private RecyclerView mRecyclerView;
    private TicketAdapter mAdapter;
    private FloatingActionButton fab;
    private View parentLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        parentLayout=findViewById(R.id.parent_lay_booking_ride);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(R.string.booking);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        loadLayout=findViewById(R.id.booking_loading);
        errorLayout=findViewById(R.id.booking_error);
        tvErrorMsg=errorLayout.findViewById(R.id.tv_error);
        contentLayout=findViewById(R.id.booking_content);

        fab=findViewById(R.id.booking_fab);
        fab.setOnClickListener(this);

        CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();




        // init view
        errorLayout.setVisibility(View.INVISIBLE);
        loadLayout.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.INVISIBLE);

        p.setAnchorId(View.NO_ID);
        fab.setLayoutParams(p);
        fab.setVisibility(View.GONE);


        // make request
        SharedPreferenceManager sMgr=new SharedPreferenceManager();
        if( sMgr.retrieveParentJWT(this)!=null|sMgr.retrieveParentJWT(this).equals("")){
            APIcallManager apiMgr=new APIcallManager();
            apiMgr.getAllBookingForParentToken(this,sMgr.retrieveParentJWT(this),this);
        }else{
            tvErrorMsg.setText("Not Signed In");
            errorLayout.setVisibility(View.VISIBLE);
            loadLayout.setVisibility(View.INVISIBLE);
            contentLayout.setVisibility(View.INVISIBLE);
            p.setAnchorId(View.NO_ID);
            fab.setLayoutParams(p);
            fab.setVisibility(View.GONE);
        }


        //getting the recyclerview from xml
        mRecyclerView = contentLayout.findViewById(R.id.tickets_recycler_view);
        //mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        //set adapter to recyclerview
        mAdapter = new TicketAdapter(DataProviderHelper.getTickets(this),this);
        mRecyclerView.setAdapter(mAdapter);
       /* mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                       // Toast.makeText(BookingActivity.this,"fab",Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(BookingActivity.this,RideDetailsActivity.class));
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );*/


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Intent i = new Intent(BookingActivity.this, BookingActivity.class);  //your class
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_book_ride, menu);
        refresh = menu.findItem(R.id.action_refresh);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }if(item.getItemId()== R.id.action_refresh){
            onRestart();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        SharedPreferenceManager sMgr=new SharedPreferenceManager();
        if( sMgr.retrieveParentJWT(this)!=null|sMgr.retrieveParentJWT(this).equals("")) {
            APIcallManager apiMgr = new APIcallManager();

            apiMgr.getSchoolList(this,sMgr.retrieveParentJWT(this), schoolListListener);
        }
    }

    @Override
    public void onSuccess(List<Ticket> list) {
//set adapter to recyclerview

        if(list.size()<=0){
            // init view
            errorLayout.setVisibility(View.VISIBLE);
            loadLayout.setVisibility(View.INVISIBLE);
            contentLayout.setVisibility(View.INVISIBLE);
            fab.setVisibility(View.VISIBLE);
            tvErrorMsg.setText("No Bookings found");
        }else {

            mAdapter = new TicketAdapter(list, this);
            mRecyclerView.setAdapter(mAdapter);

            // init view
            errorLayout.setVisibility(View.INVISIBLE);
            loadLayout.setVisibility(View.INVISIBLE);
            contentLayout.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFailure(String msg) {
        // init view
        tvErrorMsg.setText(msg);
        errorLayout.setVisibility(View.VISIBLE);
        loadLayout.setVisibility(View.INVISIBLE);
        contentLayout.setVisibility(View.INVISIBLE);
        fab.setVisibility(View.VISIBLE);
    }

    APIcallManager.SchoolApiStatus schoolListListener =new APIcallManager.SchoolApiStatus() {
        @Override
        public void onSuccess(List<School> list) {
            Log.d("tag","onSuccess");
            SharedPreferenceManager sMgr=new SharedPreferenceManager();
            if( sMgr.retrieveParentJWT(BookingActivity.this)!=null|sMgr.retrieveParentJWT(BookingActivity.this).equals("")) {
                APIcallManager apiCallManager=new APIcallManager();
                apiCallManager.getMeetingPoint(BookingActivity.this,sMgr.retrieveParentJWT(BookingActivity.this),meetingPointStatus);
            }

        }

        @Override
        public void onFailure(String msg) {
            Log.d("tag","onFailure");
            Helper.showErrorDialog(BookingActivity.this,"Could not load form, Try later!");

        }
    };
    APIcallManager.MeetingPointStatus meetingPointStatus=new APIcallManager.MeetingPointStatus() {

        @Override
        public void onMeetingPointSuccess(List<MeetingPoint> list) {
            startActivity(new Intent(BookingActivity.this,BookRideActivity.class));
            finish();
        }

        @Override
        public void onMeetingPointFailure(String msg) {
            Helper.showErrorDialog(BookingActivity.this,"Could not load form, Try later!");
        }
    };

    @Override
    public void onBookingDeleteSuccess() {
        recreate();
    }

    @Override
    public void onBookingDeleteFailure(String msg) {
       Helper.showSnackBar(this,msg,parentLayout);
    }
}
