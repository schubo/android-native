package schoolbus.ego.com.goschool.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import schoolbus.ego.com.goschool.R;

public class ChildDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_details);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.view_child);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }
}
