package schoolbus.ego.com.goschool.manager;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferenceManager {
    final int mode = MODE_PRIVATE;
    final String PREF_TAG = "com.schubo.app";
    private final String TAG_PARENT_TOKEN="PARENT_TOKEN";
    private final String TAG_CHILD_TOKEN="CHILD_TOKEN";
    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor editor;

    public void storeParentJWT(Context context, String token){
        sharedPreferences = context.getSharedPreferences(PREF_TAG, 0);
        editor = sharedPreferences.edit();

        editor.putString(TAG_PARENT_TOKEN,token );
        editor.putString(TAG_CHILD_TOKEN,null);
        editor.apply();
    }
    public  String retrieveParentJWT(Context context){
        SharedPreferences shared = context.getSharedPreferences(PREF_TAG, MODE_PRIVATE);
        return  (shared.getString(TAG_PARENT_TOKEN, ""));
    }
    public String retrieveChildJWT(Context context){
        SharedPreferences shared = context.getSharedPreferences(PREF_TAG, MODE_PRIVATE);
        return  (shared.getString(TAG_CHILD_TOKEN, ""));
    }
    public void storeChildJWT(Context context, String token){
        sharedPreferences = context.getSharedPreferences(PREF_TAG, 0);
        editor = sharedPreferences.edit();

        editor.putString(TAG_CHILD_TOKEN,token );
        editor.putString(TAG_PARENT_TOKEN,null);
        editor.apply();
    }
}
