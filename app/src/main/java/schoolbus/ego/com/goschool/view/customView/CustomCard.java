package schoolbus.ego.com.goschool.view.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import schoolbus.ego.com.goschool.R;

public class CustomCard extends LinearLayout {

    private String TAG = this.getClass().getSimpleName();
    private LayoutInflater mInflater;
    private TextView tv_title;
    private ImageView iv_Icon;
private String mTitle;
private Drawable mIcon;
    public CustomCard(Context context) {
        super(context,null);
    }

    public CustomCard(Context context,  AttributeSet attrs) {
        super(context, attrs);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomCard,
                0, 0);
        //Reading values from XML layout.
        try {
            if (typedArray.getString(R.styleable.CustomCard_title) != null)
                mTitle = typedArray.getString(R.styleable.CustomCard_title);
            if (typedArray.getString(R.styleable.CustomCard_icon) != null)
                mIcon = typedArray.getDrawable(R.styleable.CustomCard_icon);
        } finally {
            typedArray.recycle();
        }



        View root = mInflater.inflate(R.layout.layout_card, this, true);
        tv_title = (TextView) root.findViewById(R.id.tvCardTitle);
        iv_Icon = (ImageView) root.findViewById(R.id.ivCardImage);

        tv_title.setText(mTitle);
        iv_Icon.setImageDrawable(mIcon);
    }


    public void setTv_title(String title) {
        tv_title.setText(title);
    }


    public void setIv_Icon(Drawable icon) {
        iv_Icon.setImageDrawable(icon);
    }
}
