package schoolbus.ego.com.goschool.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.DashboardAdapter;
import schoolbus.ego.com.goschool.manager.SharedPreferenceManager;
import schoolbus.ego.com.goschool.model.CardItem;
import schoolbus.ego.com.goschool.utils.GridSpacingItemDecoration;
import schoolbus.ego.com.goschool.utils.Helper;
import schoolbus.ego.com.goschool.utils.RecyclerItemClickListener;

public class ChildDashboardActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DashboardAdapter adapter;
    private List<CardItem> menuList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_dashboard);
        recyclerView=findViewById(R.id.child_dash_recycler_view);



        menuList=new ArrayList<>();
        menuList.add(new CardItem(getDrawable(R.drawable.ic_qr_scan),"SCAN QR Code"));
        menuList.add(new CardItem(getDrawable(R.drawable.ic_on_boarding),"ON-BOARDING"));
        menuList.add(new CardItem(getDrawable(R.drawable.ic_feedback),"FEEDBACK"));
        menuList.add(new CardItem(getDrawable(R.drawable.ic_settings),"SETTINGS"));


        adapter = new DashboardAdapter(this, menuList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, Helper.dpToPx(this,10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        switch (position){
                            case 0:

                                startActivity(new Intent(ChildDashboardActivity.this,ScanQRCode.class));
                                break;
                            case 1:
                                Toast.makeText(ChildDashboardActivity.this,"Under Progress",Toast.LENGTH_SHORT).show();
                                //startActivity(new Intent(ChildDashboardActivity.this,BookingActivity.class));
                                break;
                            case 2:
                                Toast.makeText(ChildDashboardActivity.this,"Under Progress",Toast.LENGTH_SHORT).show();
                               // startActivity(new Intent(ChildDashboardActivity.this,TrackingActivity.class));
                                break;
                            case 3:
                                Toast.makeText(ChildDashboardActivity.this,"Under Progress",Toast.LENGTH_SHORT).show();
                               // startActivity(new Intent(ChildDashboardActivity.this,SettingsActivity.class));
                                break;
                        }
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

    }
}
