package schoolbus.ego.com.goschool.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Ride {
    private String date;
    private int trip_type;
    private int meeting_point_id;
    private List<String> child_ids=new ArrayList<>();

    public Ride(String date, int way, int point,List<String> child_ids) {
        this.date = date;
        this.trip_type = way;
        this.meeting_point_id=point;
        this.child_ids = child_ids;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTrip_type() {
        return trip_type;
    }

    public void setTrip_type(int trip_type) {
        this.trip_type = trip_type;
    }

    public int getMeeting_point_id() {
        return meeting_point_id;
    }

    public void setMeeting_point_id(int meeting_point_id) {
        this.meeting_point_id = meeting_point_id;
    }

    public List<String> getChild_ids() {
        return child_ids;
    }

    public void setChild_ids(List<String> child_ids) {
        this.child_ids = child_ids;
    }


    public JSONObject toJSON() {
        String input="{" +
                "date='" + date + '\'' +
                ", trip_type=" + trip_type +
                ", meeting_point_id=" + meeting_point_id +
                ", child_ids=" + child_ids +
                '}';
        try {
            JSONObject obj=new JSONObject(input);
            return obj;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
