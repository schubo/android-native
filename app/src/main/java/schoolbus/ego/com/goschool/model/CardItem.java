package schoolbus.ego.com.goschool.model;

import android.graphics.drawable.Drawable;

public class CardItem {
    Drawable icon;
    String title;

    public CardItem(Drawable icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
