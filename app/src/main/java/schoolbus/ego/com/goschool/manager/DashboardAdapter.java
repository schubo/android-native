package schoolbus.ego.com.goschool.manager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.model.CardItem;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {

    private Context mContext;
    private List<CardItem> menuList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.tvCardTitle);
            thumbnail = (ImageView) view.findViewById(R.id.ivCardImage);
        }
    }


    public DashboardAdapter(Context mContext, List<CardItem> menuList) {
        this.mContext = mContext;
        this.menuList = menuList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        CardItem item = menuList.get(position);
        holder.title.setText(item.getTitle());
        holder.thumbnail.setImageDrawable(item.getIcon());
    }



    @Override
    public int getItemCount() {
        return menuList.size();
    }
}

