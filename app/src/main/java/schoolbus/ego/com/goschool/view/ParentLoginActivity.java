package schoolbus.ego.com.goschool.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.APIcallManager;
import schoolbus.ego.com.goschool.manager.SharedPreferenceManager;
import schoolbus.ego.com.goschool.model.UserModel;
import schoolbus.ego.com.goschool.presenter.ParentLoginPresenter;
import schoolbus.ego.com.goschool.utils.Helper;

public class ParentLoginActivity extends AppCompatActivity implements View.OnClickListener,ParentLoginPresenter.View, APIcallManager.LoginStatus  {
   private final String TAG=this.getClass().getSimpleName();
   private EditText userName;
    private EditText password;
    private TextView errorUserName;
    private TextView errorPAssword;
    private Button btnLogin;
    private ProgressDialog dialog;
    private ParentLoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_login);
        presenter = new ParentLoginPresenter(this,this,this);
        dialog = new ProgressDialog(this);
        userName=findViewById(R.id.username);
        password=findViewById(R.id.password);
        errorUserName=findViewById(R.id.error_userName);
        errorPAssword=findViewById(R.id.error_password);
        btnLogin=findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login:

           if(Helper.isNetworkAvailable(this)){
               clearError();
               presenter.updateEmail(userName.getText().toString());
               presenter.updatePassword(password.getText().toString());
               presenter.performLogin();
           }else {
               AlertDialog alertDialog = new AlertDialog.Builder(ParentLoginActivity.this).create();
               alertDialog.setTitle("Internet not Available");
               alertDialog.setMessage("Please turn on data");
               alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                       new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int which) {
                               dialog.dismiss();
                           }
                       });
               alertDialog.show();
           }

                break;
        }
    }

    private void clearError() {
        errorPAssword.setText("");
        errorUserName.setText("");
    }

    @Override
    public void setUserNameError(String info) {
        errorUserName.setText(info);
    }

    @Override
    public void setPasswordError(String info) {
         errorPAssword.setText(info);
    }

    @Override
    public void showProgressBar() {
        dialog.setMessage("Authenticating...");
        dialog.show();
    }

    @Override
    public void showMessage(String msg) {
       dialog.setMessage(msg);
    }

    @Override
    public void hideProgressBar() {
      dialog.hide();

    }

    @Override
    public void onLoginSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(dialog!=null){
                    dialog.setMessage("Login Success");
                }

                //get Firebase token
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( ParentLoginActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String newToken = instanceIdResult.getToken();
                        Log.e("newToken",newToken);
                        APIcallManager mgr=new APIcallManager();
                        try {
                            SharedPreferenceManager sMgr = new SharedPreferenceManager();
                            if (sMgr.retrieveParentJWT(ParentLoginActivity.this) != null | sMgr.retrieveParentJWT(ParentLoginActivity.this).equals("")) {
                                mgr.registerDevice(ParentLoginActivity.this, sMgr.retrieveParentJWT(ParentLoginActivity.this),newToken, new APIcallManager.DeviceRegistrationStatus() {
                                    @Override
                                    public void onSuccess(UserModel user) {
                                        if (dialog != null) {
                                            dialog.setMessage("Device Registered for Notification");
                                            dialog.dismiss();

                                            finish();
                                            startActivity(new Intent(ParentLoginActivity.this, ParentDashboardActivity.class));
                                        }
                                    }

                                    @Override
                                    public void onFailure(String msg) {
                                        if (dialog != null) {
                                            dialog.setMessage("Device Registered failed");
                                            dialog.dismiss();
                                            showErrorDialog(ParentLoginActivity.this, "Device Registration failed, Retry");


                                        }
                                    }
                                });
                            }
                            }catch(Exception ex){
                                ex.printStackTrace();
                                if (dialog != null) {
                                    dialog.setMessage("Device Registration Server call failed");
                                    dialog.dismiss();
                                    showErrorDialog(ParentLoginActivity.this, "Device Registration failed, Retry");
                                }



                        }
                    }
                });
                FirebaseInstanceId.getInstance().getInstanceId().addOnFailureListener(ParentLoginActivity.this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.setMessage("Pairing Failed due to device token");
                        dialog.dismiss();
                        showErrorDialog(ParentLoginActivity.this,"Device Registration failed, Retry");
                    }
                });
            }
        });

    }

    @Override
    public void onLoginError(final String msg) {
        Log.d(TAG,"onLoginError");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

        if(dialog!=null){
            dialog.hide();
        }
               showErrorDialog(ParentLoginActivity.this,"Could not Login..Try again");
            }
        });
    }

    private void showErrorDialog(Context context, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        userName.setText("");
                        password.setText("");
                        clearError();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onUserInfoSuccess() {

    }

    @Override
    public void onUserInfoFailure() {

    }
}
