package schoolbus.ego.com.goschool.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.VolleySingleton;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.School;

public class ViewRegisteredChild extends AppCompatActivity {
     private String name;
     private TextView et_Name;
     private String id;
     private TextView et_id;
     private String school;
     private TextView et_school;
     private Drawable image;
     private NetworkImageView iv_Dp;
     private ImageView qrCode;
     private boolean active;
     private TextView tvStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.childdetails_activity);
        et_Name=findViewById(R.id.child_name);
        et_id=findViewById(R.id.ev_studentId);
        et_school=findViewById(R.id.et_school_name);
        iv_Dp=findViewById(R.id.profile_image);
        qrCode=findViewById(R.id.iv_child_qr_code);
        tvStatus=findViewById(R.id.tv_status);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(R.string.view_child);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if(getIntent()!=null) {
                name=getIntent().getStringExtra(Child.TAG_NAME);
                et_Name.setText(name);
                id = getIntent().getStringExtra(Child.TAG_ID);
                et_id.setText(id);
                generateQRCodeforChild(id);
                et_school.setText(getIntent().getStringExtra(Child.TAG_SCHOOL));
             String url=getIntent().getStringExtra(Child.TAG_IMAGE);
                iv_Dp.setImageUrl(url,VolleySingleton.getInstance(this).getImageLoader());
                active=getIntent().getBooleanExtra(Child.TAG_ACTIVE,false);
                if(active){
                    tvStatus.setText("Active");
                }else {
                    tvStatus.setText("Inactive");
                }


        }


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
    private void generateQRCodeforChild(String text){

        if(text!=null){
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try{
                BitMatrix bitMatrix = multiFormatWriter.encode(text,BarcodeFormat.QR_CODE, 250,250);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                qrCode.setImageBitmap(bitmap);
            } catch (WriterException e){
                e.printStackTrace();
            }
        }
    }

}
