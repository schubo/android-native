package schoolbus.ego.com.goschool.utils;

public class CustomException extends Exception {
    public CustomException(String message) {
        super(message);
    }
}
