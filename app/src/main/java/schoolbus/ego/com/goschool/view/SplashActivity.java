package schoolbus.ego.com.goschool.view;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.agrawalsuneet.dotsloader.loaders.LinearDotsLoader;
import com.auth0.android.jwt.DecodeException;
import com.auth0.android.jwt.JWT;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.SharedPreferenceManager;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT=2000;
    private LinearLayout dotLoader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        dotLoader=findViewById(R.id.dot_loader);


        LinearDotsLoader loader = new LinearDotsLoader(this);
        loader.setDefaultColor(ContextCompat.getColor(this, R.color.loader_defalut));
        loader.setSelectedColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        loader.setSingleDir(true);
        loader.setNoOfDots(3);
        loader.setSelRadius(20);
        loader.setExpandOnSelect(true);
        loader.setRadius(15);
        loader.setDotsDistance(20);
        loader.setAnimDur(300);
        loader.setShowRunningShadow(true);
        loader.setFirstShadowColor(ContextCompat.getColor(this, R.color.colorPrimary));
        loader.setSecondShadowColor(ContextCompat.getColor(this, R.color.colorAccent));

        dotLoader.addView(loader);

        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}

        new Handler().postDelayed(new Runnable() {

            @Override

            public void run() {

               /* Intent i = new Intent(SplashActivity.this, LoginActivity.class);

                startActivity(i);

                // close this activity

                finish();*/
               SharedPreferenceManager mgr=new SharedPreferenceManager();

               if(!mgr.retrieveParentJWT(SplashActivity.this).equals("")){
                   try{
                      /* JWT jwt = new JWT(mgr.retrieveParentJWT(SplashActivity.this));
                       boolean isExpired = jwt.isExpired(10);
                       if(isExpired){
                           Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                           startActivity(i);
                       }else {*/
                           Intent i = new Intent(SplashActivity.this, ParentDashboardActivity.class);
                           startActivity(i);
                           finish();
                      // }

                   }catch (DecodeException ex){
                       Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                       startActivity(i);
                       finish();
                   }

               }else if(!mgr.retrieveChildJWT(SplashActivity.this).equals("")){
                   try{
                      /* JWT jwt = new JWT(mgr.retrieveChildJWT(SplashActivity.this));
                       boolean isExpired = jwt.isExpired(10);
                       if(isExpired){
                           Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                           startActivity(i);
                       }else {*/
                           Intent i = new Intent(SplashActivity.this, ChildDashboardActivity.class);
                           startActivity(i);
                       finish();
                      // }

                   }catch (DecodeException ex){
                       Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                       startActivity(i);
                       finish();
                   }
               }else {
                   Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                   startActivity(i);
                   finish();
               }

            }

        }, SPLASH_TIME_OUT);

    }
}
