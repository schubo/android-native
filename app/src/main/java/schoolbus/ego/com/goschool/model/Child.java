package schoolbus.ego.com.goschool.model;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class Child implements Parcelable {
    public static String TAG_OBJECT="OBJECT";
    public static String TAG_NAME="NAME";
    public static String TAG_SCHOOL="SCHOOL";
    public static String TAG_ID="ID";
    public static String TAG_IMAGE="IMAGE";
    public static String TAG_ACTIVE="ACTIVE";


    private String name;
    private int stage;
    private boolean active;
    private String avatarURL;
    private String schoolName;
    private String id;
    private School school;
    private List<Parents> parents;

public Child() {

}
    public Child(String name, String schoolName, String id, Drawable displayImage) {
        this.name = name;
        this.schoolName = schoolName;
        this.id = id;
    }

    protected Child(Parcel in) {
        name = in.readString();
        stage = in.readInt();
        active = in.readByte() != 0;
        avatarURL = in.readString();
        schoolName = in.readString();
        id = in.readString();
        school = (School) in.readParcelable(School.class.getClassLoader());
        parents = (List<Parents>) in.readParcelable(Parents.class.getClassLoader());
    }

    public static final Creator<Child> CREATOR = new Creator<Child>() {
        @Override
        public Child createFromParcel(Parcel in) {
            return new Child(in);
        }

        @Override
        public Child[] newArray(int size) {
            return new Child[size];
        }
    };

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public List<Parents> getParents() {
        return  parents;
    }

    public void setParents(List<Parents> parents) {
        this.parents = parents;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(stage);
        dest.writeValue(active);
        dest.writeString(avatarURL);
        dest.writeString(schoolName);
        dest.writeString(id);
        dest.writeParcelable(school,flags);
        dest.writeParcelable((Parcelable) parents,flags);
    }
}
