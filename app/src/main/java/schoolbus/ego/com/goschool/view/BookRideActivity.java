package schoolbus.ego.com.goschool.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.application.SchuboApp;
import schoolbus.ego.com.goschool.manager.APIcallManager;
import schoolbus.ego.com.goschool.manager.SharedPreferenceManager;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.MeetingPoint;
import schoolbus.ego.com.goschool.model.Ride;
import schoolbus.ego.com.goschool.model.School;
import schoolbus.ego.com.goschool.model.Ticket;
import schoolbus.ego.com.goschool.utils.Helper;

public class BookRideActivity extends AppCompatActivity implements View.OnClickListener {

    private View loadLayout,errorLayout,contentLayout;
    private TextView tvErrorMsg;
    private List<School> slist=new ArrayList<>();
    private Spinner spinnerSchool,spinnerChild,spinnerMeetingPoint;

    private List<Child> childList=new ArrayList<>();

    private String[] schoolList;
    private String[] meetingList;
    ArrayList<String> childStringList=new ArrayList<String>();


    // Widgets
    private static EditText etDate;
    private Button btnBookRide;
    private String[] modeTypes={"0: Meeting Point to School","1: School to Meeting Point","2: Both ways"};

    // Response
    private int modeResp=-1;
    private String childResp;
    private String schoolID;
    private int meetingPointResp=1;
   private ProgressDialog dialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_ride);
        spinnerSchool=findViewById(R.id.spinner_school);
        spinnerChild=findViewById(R.id.spinner_select_child);
        spinnerMeetingPoint=findViewById(R.id.spinner_select_meeting_point);
        btnBookRide=findViewById(R.id.btn_book_ride);
        btnBookRide.setOnClickListener(this);

         dialog=new ProgressDialog(BookRideActivity.this);

        schoolList=getSchoolList(SchuboApp.getAppInstance().getSchoolList());
        meetingList=getMeetingPointList(SchuboApp.getAppInstance().getMeetingPointList());

        spinnerSchool.setOnItemSelectedListener(schoolSelectedListener);
        spinnerChild.setOnItemSelectedListener(childSelectedListener);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> schoolAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        schoolList);
// Specify the layout to use when the list of choices appears
        schoolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinnerSchool.setAdapter(schoolAdapter);

        spinnerMeetingPoint.setOnItemSelectedListener(meetingPointSelectedListener);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> meetingPointAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        meetingList);
// Specify the layout to use when the list of choices appears
        meetingPointAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinnerMeetingPoint.setAdapter(meetingPointAdapter);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(R.string.book_ride);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        loadLayout=findViewById(R.id.book_ride_loading);
        errorLayout=findViewById(R.id.book_ride_error);
        tvErrorMsg=errorLayout.findViewById(R.id.tv_error);
        contentLayout=findViewById(R.id.book_ride_content);
        initWidgets(contentLayout);

       makeChildrenListRequest();
        loadingLayoutVisisble();
    }

    private String[] getSchoolList(List<School> schoolList) {
        String[] array=new String[schoolList.size()];
        for(int i=0;i<schoolList.size();i++){
            array[i]=schoolList.get(i).getName();
        }
        return array;
    }
    private String[] getMeetingPointList(List<MeetingPoint> meetingList) {
        String[] array=new String[meetingList.size()];
        for(int i=0;i<meetingList.size();i++){
            array[i]=meetingList.get(i).getName();
        }
        return array;
    }

    private void loadingLayoutVisisble() {
        errorLayout.setVisibility(View.INVISIBLE);
        loadLayout.setVisibility(View.VISIBLE);
        contentLayout.setVisibility(View.INVISIBLE);
    }

    private void makeChildrenListRequest(){
        final ProgressDialog dialog=new ProgressDialog(this);
        dialog.setMessage("Retrieving Children..");
        dialog.show();
        SharedPreferenceManager sMgr=new SharedPreferenceManager();
        if( sMgr.retrieveParentJWT(this)!=null|sMgr.retrieveParentJWT(this).equals("")){
            APIcallManager apiMgr=new APIcallManager();
            apiMgr.getAllChildren(this, sMgr.retrieveParentJWT(this), new APIcallManager.ChildrenApiStatus() {
                @Override
                public void onSucess(List<Child> list) {
                    dialog.dismiss();
                    childList=list;
                    contentLayoutVisible();


                }

                @Override
                public void onFailure(String msg) {
                    dialog.dismiss();
                    Helper.showErrorDialog(BookRideActivity.this,"Could not retrieve Children, try later..");
                }
            });
        }else{
            errorLayoutVisible("Access token not found");
        }
    }

    private void errorLayoutVisible(String msg) {
        errorLayout.setVisibility(View.VISIBLE);
        loadLayout.setVisibility(View.INVISIBLE);
        contentLayout.setVisibility(View.INVISIBLE);
        tvErrorMsg.setText(msg);
    }
    private void contentLayoutVisible(){
        errorLayout.setVisibility(View.INVISIBLE);
        loadLayout.setVisibility(View.INVISIBLE);
        contentLayout.setVisibility(View.VISIBLE);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
    private void initWidgets(View contentLayout) {
        etDate=contentLayout.findViewById(R.id.et_ride_date);
        etDate.setOnClickListener(this);
        Spinner spin = (Spinner) findViewById(R.id.spinner_mode);
        spin.setOnItemSelectedListener(modeSelectedListener);
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,modeTypes);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(aa);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.et_ride_date:
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
                break;

            case R.id.btn_book_ride:
                if(modeResp!=-1&&(childResp!=null&&!childResp.isEmpty())&&(schoolID!=null&&!schoolID.isEmpty())){

                    dialog.setMessage("Booking a Ride...");
                    dialog.show();
                    APIcallManager apiMgr=new APIcallManager();
                    List<String> list=new ArrayList<>();
                    list.add(childResp);
                    Ride ride=new Ride(etDate.getText().toString(),modeResp,meetingPointResp,list);

                    SharedPreferenceManager sMgr=new SharedPreferenceManager();
                    if( sMgr.retrieveParentJWT(this)!=null|sMgr.retrieveParentJWT(this).equals("")){
                        apiMgr.createBooking(this,sMgr.retrieveParentJWT(this),ride,bookingApiStatus);
                    }else{
                       Helper.showErrorDialog(this,"Please SignIn, token missing");
                    }

                }else {
                    Helper.showErrorDialog(this,"Please Fill all fields");
                }
                break;
        }

    }
APIcallManager.CreateBookingApiStatus bookingApiStatus=new APIcallManager.CreateBookingApiStatus() {
    @Override
    public void onSuccess() {
        dialog.setMessage("Booking Successful");
        dialog.dismiss();
        Helper.showErrorDialog(BookRideActivity.this,"Booking Done");
    }

    @Override
    public void onFailure(String msg) {
        dialog.setMessage("Booking Failed");
        dialog.dismiss();
        Helper.showErrorDialog(BookRideActivity.this,"Booking Failed, Try Later..");
    }
};


    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            etDate.setText(day + "/" + (month + 1) + "/" + year);
        }

    }

    AdapterView.OnItemSelectedListener schoolSelectedListener=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            schoolID= String.valueOf(SchuboApp.getAppInstance().getSchoolList().get(position).getId());
            childStringList.clear();
            for(int i=0;i<childList.size();i++){
                if(childList.get(i).getSchool()!=null&&childList.get(i).getSchool().getId()!=-1){
                    if(childList.get(i).getSchool().getId()==Integer.parseInt(schoolID)){
                        childStringList.add(childList.get(i).getName());
                    }
                }
            }
            // Create an ArrayAdapter using the string array and a default spinner layout

            String[] myArray = new String[childStringList.size()];
            childStringList.toArray(myArray);
            ArrayAdapter<String> childAdapter = new ArrayAdapter<String>
                    (BookRideActivity.this, android.R.layout.simple_spinner_item,
                            myArray);
// Specify the layout to use when the list of choices appears
            childAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
            childAdapter.notifyDataSetInvalidated();
            spinnerChild.setAdapter(childAdapter);


        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            schoolID= String.valueOf(SchuboApp.getAppInstance().getSchoolList().get(0).getId());

        }
    };

    AdapterView.OnItemSelectedListener modeSelectedListener=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
          modeResp=position;

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            modeResp=0;

        }
    };
    AdapterView.OnItemSelectedListener meetingPointSelectedListener=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            MeetingPoint point=SchuboApp.getAppInstance().getMeetingPointList().get(position);
           meetingPointResp=point.getId();

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
           meetingPointResp=1;

        }
    };

    AdapterView.OnItemSelectedListener childSelectedListener=new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String name=childStringList.get(position);
            for(int i=0;i<childList.size();i++){
                if(childList.get(i).getName().equalsIgnoreCase(name)){
                    childResp=childList.get(i).getId();
                }
            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
         childResp="";
        }
    };


}
