package schoolbus.ego.com.goschool.view.customView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import android.widget.AdapterView.OnItemSelectedListener;

import java.io.IOException;
import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.application.SchuboApp;
import schoolbus.ego.com.goschool.manager.APIcallManager;
import schoolbus.ego.com.goschool.manager.SharedPreferenceManager;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.School;
import schoolbus.ego.com.goschool.utils.Helper;

public class ChildRegistrationForm extends AppCompatActivity implements View.OnClickListener {

    private Button choosePhoto,generateQRCode;
    private ImageView profilePic, qrCode;
    private EditText childName,studentId;
    private Spinner schoolName,spinnerStage;
    private TextView errorChildName,errorStudentId, errorSchoolName;
    private static final int PICK_IMG=100;
    private Uri imageUri;
    private String[] schoolList;
    private String[] stageList ={"1","2","3","4","5", "6","7"};

    // Response
    private String schoolID;
    private String stage;
    private String name;
    private Drawable icon;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_registration_form);
        schoolList=getSchoolList(SchuboApp.getAppInstance().getSchoolList());
        //Spinner element
        schoolName = (Spinner) findViewById(R.id.et_school_name);
        spinnerStage=(Spinner) findViewById(R.id.spinner_stage);
        schoolName.setOnItemSelectedListener(schoolSelectedListener);
        spinnerStage.setOnItemSelectedListener( stageListener);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> schoolAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        schoolList);
// Specify the layout to use when the list of choices appears
        schoolAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        schoolName.setAdapter(schoolAdapter);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> stageAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item,
                        stageList);
// Specify the layout to use when the list of choices appears
        stageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinnerStage.setAdapter(stageAdapter);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(R.string.add_child);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        childName=findViewById(R.id.et_child_name);
        studentId=findViewById(R.id.et_student_id);
        schoolName=findViewById(R.id.et_school_name);
        profilePic=findViewById(R.id.iv_child_pic);
        errorChildName=findViewById(R.id.tv_error_childName);
        errorStudentId=findViewById(R.id.tv_error_student_id);
        errorSchoolName=findViewById(R.id.tv_error_schoolName);
        choosePhoto=findViewById(R.id.btn_choose_photo);
        generateQRCode=findViewById(R.id.btn_generate_qr_code);
        qrCode=findViewById(R.id.imageView);
        choosePhoto.setOnClickListener(this);

        generateQRCode.setOnClickListener(this);

    }

    private String[] getSchoolList(List<School> schoolList) {
        String[] array=new String[schoolList.size()];
        for(int i=0;i<schoolList.size();i++){
            array[i]=schoolList.get(i).getName();
        }
        return array;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_choose_photo:
                openGallery();
                break;
            case R.id.btn_generate_qr_code:
                if(icon==null){
                    Helper.showErrorDialog(ChildRegistrationForm.this,"Select a Photo");
                }else
                if(TextUtils.isEmpty(childName.getText())) {errorChildName.requestFocus(); errorChildName.setError("Enter child name");}
                else
                {
                    name=childName.getText().toString();
                    final ProgressDialog dialog=new ProgressDialog(ChildRegistrationForm.this);
                   // dialog.setCancelable(false);
                    dialog.setMessage("Registering child..");
                    dialog.show();
// make request
                SharedPreferenceManager sMgr=new SharedPreferenceManager();
                if( sMgr.retrieveParentJWT(this)!=null|sMgr.retrieveParentJWT(this).equals("")) {
                    APIcallManager apiMgr = new APIcallManager();
                    apiMgr.createChild(this, sMgr.retrieveParentJWT(this), name, stage, schoolID, icon, new APIcallManager.CreateChildApiStatus() {
                        @Override
                        public void onSuccess(Child child) {
                            if(dialog!=null){
                                dialog.setMessage("Registration Success");
                                dialog.dismiss();
                            }
                            generateQRCodeforChild(child.getId());
                        }

                        @Override
                        public void onFailure(String msg) {
                            if(dialog!=null){
                                dialog.dismiss();
                                Helper.showErrorDialog(ChildRegistrationForm.this,"Child Registration Failed, Try again..");
                            }

                        }
                    });

                    break;
                }

                }
                break;
        }
    }

    private void openGallery() {
        Intent intent = new Intent();
// Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),PICK_IMG);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMG && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                icon = new BitmapDrawable(getResources(), bitmap);
                profilePic.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPanelClosed(int featureId, Menu menu) {
        super.onPanelClosed(featureId, menu);
    }

    private void generateQRCodeforChild(String text){

        if(text!=null){
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try{
                BitMatrix bitMatrix = multiFormatWriter.encode(text,BarcodeFormat.QR_CODE, 500,500);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                qrCode.setImageBitmap(bitmap);
            } catch (WriterException e){
                e.printStackTrace();
            }
        }
    }
    OnItemSelectedListener schoolSelectedListener=new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            schoolID= String.valueOf(SchuboApp.getAppInstance().getSchoolList().get(position).getId());
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            schoolID= String.valueOf(SchuboApp.getAppInstance().getSchoolList().get(0).getId());

        }
    };
    OnItemSelectedListener stageListener=new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              stage=stageList[position];
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            stage=stageList[0];
        }
    };

}
