package schoolbus.ego.com.goschool.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.DashboardAdapter;
import schoolbus.ego.com.goschool.model.CardItem;
import schoolbus.ego.com.goschool.utils.GridSpacingItemDecoration;
import schoolbus.ego.com.goschool.utils.Helper;
import schoolbus.ego.com.goschool.utils.RecyclerItemClickListener;

public class ParentDashboardActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DashboardAdapter adapter;
    private List<CardItem> menuList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_dashboard);
        recyclerView=findViewById(R.id.recycler_view);


        menuList=new ArrayList<>();
        menuList.add(new CardItem(getDrawable(R.drawable.ic_add_icon),"MANAGE CHILDREN"));
        menuList.add(new CardItem(getDrawable(R.drawable.ic_booking),"BOOKING"));
        menuList.add(new CardItem(getDrawable(R.drawable.ic_track),"TRACKING"));
        menuList.add(new CardItem(getDrawable(R.drawable.ic_settings),"SETTINGS"));


        adapter = new DashboardAdapter(this, menuList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, Helper.dpToPx(this,10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                       switch (position){
                           case 0:
                               startActivity(new Intent(ParentDashboardActivity.this,ConfigureChildActivity.class));
                               break;
                           case 1:
                               startActivity(new Intent(ParentDashboardActivity.this,BookingActivity.class));
                               break;
                           case 2:
                               startActivity(new Intent(ParentDashboardActivity.this,BusTrackingActivity.class));
                               break;
                           case 3:
                               startActivity(new Intent(ParentDashboardActivity.this,SettingsActivity.class));
                               break;
                       }
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

    }
}
