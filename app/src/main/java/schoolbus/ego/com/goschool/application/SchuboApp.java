package schoolbus.ego.com.goschool.application;

import android.app.Application;

import com.android.volley.RequestQueue;

import java.util.List;

import schoolbus.ego.com.goschool.manager.VolleySingleton;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.MeetingPoint;
import schoolbus.ego.com.goschool.model.School;
import schoolbus.ego.com.goschool.model.UserModel;
import schoolbus.ego.com.goschool.utils.DataProviderHelper;

public class SchuboApp extends Application {

    private List<Child> childList;

    private static SchuboApp instance;
    private RequestQueue requestQueue;
    private UserModel user;
    private List<School> schoolList;
    private List<MeetingPoint> meetingPointList;

    public static SchuboApp getAppInstance() { return instance; }
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        childList= DataProviderHelper.getChildList(this);
        requestQueue = VolleySingleton.getInstance(getApplicationContext()).getRequestQueue();
    }
    public RequestQueue getVolleyRequestQueue(){
        return requestQueue;
    }
    public List<Child> getChildList() {
        return childList;
    }

    public void setChildList(List<Child> childList) {
        this.childList = childList;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public List<School> getSchoolList() {
        return schoolList;
    }

    public void setSchoolList(List<School> schoolList) {
        this.schoolList = schoolList;
    }

    public List<MeetingPoint> getMeetingPointList() {
        return meetingPointList;
    }

    public void setMeetingPointList(List<MeetingPoint> meetingPointList) {
        this.meetingPointList = meetingPointList;
    }
}
