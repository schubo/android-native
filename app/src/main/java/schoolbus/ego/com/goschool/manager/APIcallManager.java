package schoolbus.ego.com.goschool.manager;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import schoolbus.ego.com.goschool.application.SchuboApp;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.MeetingPoint;
import schoolbus.ego.com.goschool.model.Ride;
import schoolbus.ego.com.goschool.model.School;
import schoolbus.ego.com.goschool.model.Ticket;
import schoolbus.ego.com.goschool.model.UserModel;
import schoolbus.ego.com.goschool.utils.Helper;

public class APIcallManager {

    public void postLogin(final LoginStatus listener,final Context context, final String userName, final String passWord){
        StringRequest postRequest = new StringRequest(Request.Method.POST, ApiURL.loginURL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error instanceof NoConnectionError){
                            ConnectivityManager cm = (ConnectivityManager)context
                                    .getSystemService(Context.CONNECTIVITY_SERVICE);
                            NetworkInfo activeNetwork = null;
                            if (cm != null) {
                                activeNetwork = cm.getActiveNetworkInfo();
                            }
                            if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                               // Toast.makeText(context, "Server is not connected to internet.",Toast.LENGTH_SHORT).show();
                                listener.onLoginError("Server is not connected to internet");
                            } else {
                                //Toast.makeText(context, "Your device is not connected to internet.", Toast.LENGTH_SHORT).show();
                                listener.onLoginError("Your device is not connected to internet");
                            }
                        } else if (error instanceof NetworkError || error.getCause() instanceof ConnectException
                                || (error!=null&& error.getCause()!=null&&error.getCause().getMessage() != null
                                && error.getCause().getMessage().contains("connection"))){
                           // Toast.makeText(context, "Your device is not connected to internet.", Toast.LENGTH_SHORT).show();
                            listener.onLoginError("Your device is not connected to internet");
                        } else if (error.getCause() instanceof MalformedURLException){
                            //Toast.makeText(context, "Bad Request.", Toast.LENGTH_SHORT).show();
                            listener.onLoginError("Bad Request");
                        } else if (error instanceof ParseError || error.getCause() instanceof IllegalStateException
                                || error.getCause() instanceof JSONException
                                || error.getCause() instanceof XmlPullParserException){
                           // Toast.makeText(context, "Parse Error (because of invalid json or xml).", Toast.LENGTH_SHORT).show();
                            listener.onLoginError("Parse Error");
                        } else if (error.getCause() instanceof OutOfMemoryError){
                            //Toast.makeText(context, "Out Of Memory Error.", Toast.LENGTH_SHORT).show();
                            listener.onLoginError("Out Of Memory Error");
                        }else if (error instanceof AuthFailureError){
                           // Toast.makeText(context, "server couldn't find the authenticated request.", Toast.LENGTH_SHORT).show();
                            listener.onLoginError("server couldn't find the authenticated request");
                        } else if (error instanceof ServerError || error.getCause() instanceof ServerError) {
                           // Toast.makeText(context, "Server is not responding.", Toast.LENGTH_SHORT).show();
                            listener.onLoginError("Server is not responding");
                        }else if (error instanceof TimeoutError || error.getCause() instanceof SocketTimeoutException
                                || error.getCause() instanceof ConnectTimeoutException
                                || error.getCause() instanceof SocketException
                                || (error.getCause().getMessage() != null
                                && error.getCause().getMessage().contains("Connection timed out"))) {
                           // Toast.makeText(context, "Connection timeout error", Toast.LENGTH_SHORT).show();
                            listener.onLoginError("Connection timeout error");
                        } else {
                           // Toast.makeText(context, "An unknown error occurred.", Toast.LENGTH_SHORT).show();
                            listener.onLoginError("An unknown error occurred");
                        }
                    }
                }
        ) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    if(response.statusCode==200){
                        String token = response.headers.get("authorization");
                        SharedPreferenceManager smgr=new SharedPreferenceManager();
                        smgr.storeParentJWT(context,token);
                        listener.onLoginSuccess();
                    }

                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                return params;
            }


            @Override
            public byte[] getBody() throws AuthFailureError {
                JSONObject credentail = new JSONObject();
                JSONObject account=new JSONObject();
                try {
                    credentail.put("email", userName);
                    credentail.put("password", passWord);

                    account.put("account",credentail);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return account.toString().getBytes();
            }
        };
        VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);
    }

    public void getAllChildren(final Context context, final String token, final ChildrenApiStatus listener){
        StringRequest postRequest = new StringRequest(Request.Method.GET, ApiURL.childrenURL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        JsonParser parser=new JsonParser();
                     List<Child> list=parser.parseChildren(response);
                     listener.onSucess(list);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Response", "Error");
                       /* if(error!=null&&error.getCause()!=null){
                            listener.onFailure(error.getCause().getMessage());
                        }else {*/
                            listener.onFailure("Could not get response from server");
                       // }

                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                params.put("Authorization",token);
                return params;
            }

        };
        VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);

    }
    public void getMeetingPoint(final Context context,final String token, final MeetingPointStatus listener ){

        StringRequest postRequest = new StringRequest(Request.Method.GET, ApiURL.meetingPointURL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        JsonParser parser=new JsonParser();
                        SchuboApp.getAppInstance().setMeetingPointList(parser.parseMeetingPointList(response));
                        listener.onMeetingPointSuccess(SchuboApp.getAppInstance().getMeetingPointList());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Response", "Error");
                        listener.onMeetingPointFailure("Error getting response from server");
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                params.put("Authorization",token);
                return params;
            }

        };
        VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);

    }

    public void getSchoolList(final Context context, final String token, final SchoolApiStatus listener){
        StringRequest postRequest = new StringRequest(Request.Method.GET, ApiURL.schoolURL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        JsonParser parser=new JsonParser();
                        List<School> list=parser.parseSchoolList(response);
                        SchuboApp.getAppInstance().setSchoolList(list);
                        listener.onSuccess(list);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Response", "Error");
                       /* if(error!=null&&error.getCause()!=null){
                            listener.onFailure(error.getCause().getMessage());
                        }else {*/
                        listener.onFailure("Could not get response from server");
                        // }

                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                params.put("Authorization",token);
                return params;
            }

        };
        VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);
    }

    public void createChild(final Context context, final String token, final String name, final String stage, final String schoolID, final Drawable drawable,final CreateChildApiStatus listener){
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,ApiURL.createChildrenURL , new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    String resultResponse = new String(response.data,StandardCharsets.UTF_8);
                    Log.d("TAG",resultResponse);
                    JsonParser parser=new JsonParser();
                    Child list= parser.parseChild(resultResponse);
                    listener.onSuccess(list);
                }catch (Exception ex){
                    ex.printStackTrace();
                }


                // parse success output
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(error instanceof NoConnectionError){
                    ConnectivityManager cm = (ConnectivityManager)context
                            .getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = null;
                    if (cm != null) {
                        activeNetwork = cm.getActiveNetworkInfo();
                    }
                    if(activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
                         //Toast.makeText(context, "Server is not connected to internet.",Toast.LENGTH_SHORT).show();
                        listener.onFailure("Server is not connected to internet");
                    } else {
                        //Toast.makeText(context, "Your device is not connected to internet.", Toast.LENGTH_SHORT).show();
                        listener.onFailure("Your device is not connected to internet");
                    }
                } else if (error instanceof NetworkError || error.getCause() instanceof ConnectException
                        || (error!=null&& error.getCause()!=null&&error.getCause().getMessage() != null
                        && error.getCause().getMessage().contains("connection"))){
                     //Toast.makeText(context, "Your device is not connected to internet.", Toast.LENGTH_SHORT).show();
                    listener.onFailure("Your device is not connected to internet");
                } else if (error.getCause() instanceof MalformedURLException){
                   // Toast.makeText(context, "Bad Request.", Toast.LENGTH_SHORT).show();
                    listener.onFailure("Bad Request");
                } else if (error instanceof ParseError || error.getCause() instanceof IllegalStateException
                        || error.getCause() instanceof JSONException
                        || error.getCause() instanceof XmlPullParserException){
                    // Toast.makeText(context, "Parse Error (because of invalid json or xml).", Toast.LENGTH_SHORT).show();
                    listener.onFailure("Parse Error");
                } else if (error.getCause() instanceof OutOfMemoryError){
                    //Toast.makeText(context, "Out Of Memory Error.", Toast.LENGTH_SHORT).show();
                    listener.onFailure("Out Of Memory Error");
                }else if (error instanceof AuthFailureError){
                    // Toast.makeText(context, "server couldn't find the authenticated request.", Toast.LENGTH_SHORT).show();
                    listener.onFailure("server couldn't find the authenticated request");
                } else if (error instanceof ServerError || error.getCause() instanceof ServerError) {
                     //Toast.makeText(context, "Server is not responding.", Toast.LENGTH_SHORT).show();
                    listener.onFailure("Server is not responding");
                }else if (error instanceof TimeoutError || error.getCause() instanceof SocketTimeoutException
                        || error.getCause() instanceof ConnectTimeoutException
                        || error.getCause() instanceof SocketException
                        || (error.getCause().getMessage() != null
                        && error.getCause().getMessage().contains("Connection timed out"))) {
                    // Toast.makeText(context, "Connection timeout error", Toast.LENGTH_SHORT).show();
                    listener.onFailure("Connection timeout error");
                } else {
                    // Toast.makeText(context, "An unknown error occurred.", Toast.LENGTH_SHORT).show();
                    listener.onFailure("An unknown error occurred");
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("child[name]", name);
                params.put("child[stage]", stage);
                params.put("child[school_id]", schoolID);
                return params;
            }
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("child[avatar]", new DataPart("file_avatar.jpg", Helper.getFileDataFromDrawable(context, drawable), "image/jpeg"));


                return params;
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Authorization",token);
                return params;
            }
        };


        VolleySingleton.getInstance(context).addToRequestQueue(multipartRequest);
    }
    public void getAllBookingForParentToken(final Context context,final String token,final BookingDetailsApiStatus listener){
        StringRequest postRequest = new StringRequest(Request.Method.GET, ApiURL.bookingParentTokenURL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        JsonParser parser=new JsonParser();
                        List<Ticket> list=parser.parseBookingList(context,response);
                        listener.onSuccess(list);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Response", "Error");
                       /* if(error!=null&&error.getCause()!=null){
                            listener.onFailure(error.getCause().getMessage());
                        }else {*/
                        listener.onFailure("Could not get response from server");
                        // }

                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                params.put("Authorization",token);
                return params;
            }

        };
        VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);

    }
    public void createBooking(final Context context,final String token, final Ride ride, final CreateBookingApiStatus listener){
        StringRequest postRequest = new StringRequest(Request.Method.POST, ApiURL.createBookingURL,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                        JsonParser parser=new JsonParser();
                        listener.onSuccess();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            String resultResponse = new String(error.networkResponse.data,StandardCharsets.UTF_8);
                            Log.d("TAG",resultResponse);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                        listener.onFailure("Could not get response from server");
                    }
                }
        ) {

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json");
                params.put("Authorization",token);
                return params;
            }


            @Override
            public byte[] getBody() throws AuthFailureError {
                JSONObject booking = new JSONObject();
                try {

                    booking.put("booking", ride.toJSON());


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return booking.toString().getBytes();
            }
        };
        VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);
    }
    public void pairChildDevice(final Context context,final String childID,final String token,final PairingApiStatus listener){
        StringRequest postRequest = new StringRequest(Request.Method.POST, ApiURL.pairChildURL+childID+"/pair",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("Response", response);
                    }
                },
             new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String resultResponse = null;
                        try {
                            resultResponse = new String(error.networkResponse.data,StandardCharsets.UTF_8);
                            Log.d("TAG",resultResponse);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }


                    }
                }
        ) {
            @Override
            protected Response<String> parseNetworkResponse(final NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    if(response.statusCode==200){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                String token = response.headers.get("authorization");
                                SharedPreferenceManager smgr=new SharedPreferenceManager();
                                smgr.storeChildJWT(context,token);
                                listener.onSucess();
                            }
                        });

                    }else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                        listener.onFailure("Could not pair due to server error");
                            }
                        });
                    }

                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }



            @Override
            public byte[] getBody() throws AuthFailureError {
                JSONObject devParam = new JSONObject();
                JSONObject device=new JSONObject();
                try {
                    devParam.put("token", token);
                    devParam.put("os", "android");

                    device.put("device",devParam);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return device.toString().getBytes();
            }
        };
        VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);
    }

public void registerDevice(final Context context,final String token,final String fcmToken,final DeviceRegistrationStatus listener){
    StringRequest postRequest = new StringRequest(Request.Method.POST, ApiURL.registerDeviceURL,
            new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response) {
                    Log.d("Response", response);
                    JsonParser parser=new JsonParser();
                  UserModel user=  parser.parseUserModel(response);
                    SchuboApp.getAppInstance().setUser(user);
                    listener.onSuccess(user);

                }
            },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    listener.onFailure("Could not get response from server");

                }
            }
    ) {
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String,String> params = new HashMap<String, String>();
            params.put("Content-Type","application/json");
            params.put("Authorization",token);
            return params;
        }
        @Override
        public String getBodyContentType() {
            return "application/json; charset=utf-8";
        }



        @Override
        public byte[] getBody() throws AuthFailureError {
            JSONObject devParam = new JSONObject();
            JSONObject device=new JSONObject();
            try {
                devParam.put("token", fcmToken);
                devParam.put("os", "android");

                device.put("device",devParam);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return device.toString().getBytes();
        }
    };
    VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);
}
public void deleteChild(final Context context,final String token, final String childID,final DeleteChildApiStatus listener){
    StringRequest postRequest = new StringRequest(Request.Method.DELETE, ApiURL.deleteChildURL+childID,
            new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response) {
                    Log.d("Response", response);

                }
            },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Response", "Error");
                       /* if(error!=null&&error.getCause()!=null){
                            listener.onFailure(error.getCause().getMessage());
                        }else {*/
                    // }

                }
            }
    ) {

        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String,String> params = new HashMap<String, String>();
            params.put("Content-Type","application/json");
            params.put("Authorization",token);
            return params;
        }
        @Override
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            String responseString = "";
            if (response != null) {
                if(response.statusCode==200){
                    listener.onDeleteSuccess();
                }else {
                    listener.onDeleteFailure("Could not delete... Try later");
                }

            }
            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
        }

    }

    ;
    VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);
}
public void deleteBooking(final Context context,final String token, final int bookingID,final DeleteBookingApiStatus listener){
    StringRequest postRequest = new StringRequest(Request.Method.POST, ApiURL.deleteBookingURL+bookingID+"/cancel",
            new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response) {
                    Log.d("Response", response);
                }
            },
            new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        String resultResponse = new String(error.networkResponse.data,StandardCharsets.UTF_8);
                        Log.d("TAG",resultResponse);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
            }
    ) {

        @Override
        public String getBodyContentType() {
            return "application/json; charset=utf-8";
        }
        @Override
        public Map<String, String> getHeaders() throws AuthFailureError {
            Map<String,String> params = new HashMap<String, String>();
            params.put("Content-Type","application/json");
            params.put("Authorization",token);
            return params;
        }
        @Override
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            String responseString = "";
            if (response != null) {
                if(response.statusCode==200){
                    listener.onBookingDeleteSuccess();
                }else {
                    listener.onBookingDeleteFailure("Ride Could not be deleted");
                }

            }
            return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
        }

    };
    VolleySingleton.getInstance(context).getRequestQueue().add(postRequest);
}

public void getUserInfo(){

    }

    public interface LoginStatus{
        public void onLoginSuccess();
        public void onLoginError(String msg);
        public  void onUserInfoSuccess();
        public  void onUserInfoFailure();
    }
    public interface ChildrenApiStatus {
        public void onSucess(List<Child> list);
        public void onFailure(String msg);
    }
    public interface SchoolApiStatus{
        public void onSuccess(List<School> list);
        public void onFailure(String msg);
    }
    public interface BookingDetailsApiStatus {
        public void onSuccess(List<Ticket> list);
        public void onFailure(String msg);
    }
    public interface CreateBookingApiStatus {
        public void onSuccess();
        public void onFailure(String msg);
    }
    public interface PairingApiStatus{
        public void onSucess();
        public void onFailure(String msg);
    }
    public interface DeviceRegistrationStatus{
        public void onSuccess(UserModel user);
        public void onFailure(String msg);
    }
    public interface CreateChildApiStatus{
        public void onSuccess(Child child);
        public void onFailure(String msg);
    }
    public interface DeleteChildApiStatus{
        public void onDeleteSuccess();
        public void onDeleteFailure(String msg);
    }
    public interface DeleteBookingApiStatus{
        public void onBookingDeleteSuccess();
        public void onBookingDeleteFailure(String msg);
    }
    public interface MeetingPointStatus{
        public void onMeetingPointSuccess(List<MeetingPoint> list);
        public void onMeetingPointFailure(String msg);
    }
}
