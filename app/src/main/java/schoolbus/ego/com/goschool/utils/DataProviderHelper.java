package schoolbus.ego.com.goschool.utils;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.Ticket;
import schoolbus.ego.com.goschool.model.TrackBus;

public class DataProviderHelper  {
    public static ArrayList<Child> getChildList(Context context){
        ArrayList<Child> list=new ArrayList<>();
        list.add(new Child("Tom","School of Aachen","123", context.getDrawable(R.drawable.tom)));
        list.add(new Child("Jerry","School of Cologne","456", context.getDrawable(R.drawable.jerry)));
        return list;
    }
    public static ArrayList<Ticket> getTickets(Context context){
        ArrayList<Ticket> list=new ArrayList<>();

        try {
            list.add(new Ticket(Helper.getAddress(context,50.785942, 6.067103),Helper.getAddress(context,50.780826, 6.087567),Helper.convertToDate("2018-12-9"),"1234","gadgha"));
            list.add(new Ticket(Helper.getAddress(context,50.785942, 6.067103),Helper.getAddress(context,50.780826, 6.087567),Helper.convertToDate("2018-12-9"),"456","bhal"));

        } catch (CustomException e) {
            e.printStackTrace();
        }
         return  list;
    }
    public static TrackBus getCurrentLocationOfBus(){
        TrackBus bus=new TrackBus(50.774390, 6.086893);
        bus.setTitle("Elisen Brunen");
        return bus;
    }
}
