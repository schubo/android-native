package schoolbus.ego.com.goschool.presenter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import java.util.regex.Pattern;

import schoolbus.ego.com.goschool.manager.APIcallManager;
import schoolbus.ego.com.goschool.manager.ApiURL;
import schoolbus.ego.com.goschool.model.ParentLoginCred;
import schoolbus.ego.com.goschool.view.ParentLoginActivity;

public class ParentLoginPresenter {

    private ParentLoginCred user;
    private View view;
    private APIcallManager.LoginStatus listener;
    private Context context;

    public ParentLoginPresenter(Context context, View view, APIcallManager.LoginStatus listener) {
        this.user = new ParentLoginCred();
        this.context=context;
        this.listener=listener;
        this.view = view;
    }
    public void updateEmail(String email){
        user.setUserName(email);

    }

    public void updatePassword(String pwd){
        user.setPassWord(pwd);

    }
    public void performLogin(){
       if(!isValidEmail()){
           view.setUserNameError("Username is invalid");
       }
       if(!isValidPassword()){
           view.setPasswordError("Password is invalid");
       }
       if(isValidEmail()&&isValidPassword()){
           login();
       }
    }

    private void login() {
        view.showProgressBar();
        APIcallManager mgr=new APIcallManager();
        mgr.postLogin(listener,context,user.getUserName(),user.getPassWord());
    }

    private boolean isValidEmail() {
        return (!TextUtils.isEmpty(user.getUserName()) && Patterns.EMAIL_ADDRESS.matcher(user.getUserName()).matches());
    }

    private boolean isValidPassword() {
        /*Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");

        return !TextUtils.isEmpty(user.getPassWord()) && PASSWORD_PATTERN.matcher(user.getPassWord()).matches();*/
        return !TextUtils.isEmpty(user.getPassWord()) && (user.getPassWord().length()>=6);

        // Kiwi2017#
    }




    public interface View{

        void setUserNameError(String info);
        void setPasswordError(String info);
        void showProgressBar();
        void showMessage(String msg);
        void hideProgressBar();

    }
}
