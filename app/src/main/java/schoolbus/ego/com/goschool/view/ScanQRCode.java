package schoolbus.ego.com.goschool.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.APIcallManager;
import schoolbus.ego.com.goschool.utils.CustomException;
import schoolbus.ego.com.goschool.utils.Helper;

public class ScanQRCode extends AppCompatActivity implements View.OnClickListener{

    private View loadLayout,errorLayout,contentLayout;
    private TextView errorText;
    private FloatingActionButton fab;
    private ImageView qrCode;
    private String id;
    private int WIDTH=300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qrcode);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(R.string.pair_device);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        loadLayout=findViewById(R.id.qr_loading);
        errorLayout=findViewById(R.id.qr_error);
        errorText=errorLayout.findViewById(R.id.tv_error);
        contentLayout=findViewById(R.id.qr_content);
        qrCode=contentLayout.findViewById(R.id.iv_qr_code);
        fab=findViewById(R.id.qr_code_fab);
        fab.setOnClickListener(this);
        refreshView();


    }

    private void refreshView(){
        try {
            id= Helper.getChildId(this);
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try{
                BitMatrix bitMatrix = multiFormatWriter.encode(id,BarcodeFormat.QR_CODE, 500,500);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                qrCode.setImageBitmap(bitmap);

                // hide/unhide view
                loadLayout.setVisibility(View.INVISIBLE);
                errorLayout.setVisibility(View.INVISIBLE);
                contentLayout.setVisibility(View.VISIBLE);

            } catch (WriterException e){
                e.printStackTrace();
                // hide/unhide view
                loadLayout.setVisibility(View.INVISIBLE);
                contentLayout.setVisibility(View.INVISIBLE);
                errorLayout.setVisibility(View.VISIBLE);

            }
        } catch (CustomException e) {
            e.printStackTrace();
            loadLayout.setVisibility(View.INVISIBLE);
            contentLayout.setVisibility(View.INVISIBLE);
            errorText.setText(e.getMessage());
            errorLayout.setVisibility(View.VISIBLE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if(result.getContents()==null){
                Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_LONG).show();
                refreshView();
            }
            else {
                Toast.makeText(this, "Device Paired",Toast.LENGTH_LONG).show();
                Helper.storeChidId(this,result.getContents());
                refreshView();
            }
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }

    public void getToken(View view) {
        if (Helper.isNetworkAvailable(this)) {
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage("Trying to pair");
            dialog.show();
            //get Firebase token
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    Log.e("newToken", newToken);
                    APIcallManager mgr = new APIcallManager();
                    try {
                        mgr.pairChildDevice(ScanQRCode.this, Helper.getChildId(ScanQRCode.this), newToken, new APIcallManager.PairingApiStatus() {
                            @Override
                            public void onSucess() {
                                dialog.setMessage("Pairing Success");
                                dialog.dismiss();
                                Helper.showErrorDialog(ScanQRCode.this, "Device Paired Successfully");
                                Toast.makeText(ScanQRCode.this, "Pairing Success", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(String msg) {
                                dialog.setMessage("Pairing Failed due to server issue");
                                Helper.showErrorDialog(ScanQRCode.this, "Pairing Failed due to server issue");
                                Toast.makeText(ScanQRCode.this, "Pairing Failed", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        dialog.setMessage("Pairing Failed due to chid ID");
                        Helper.showErrorDialog(ScanQRCode.this, "Pairing Failed due to chid ID");
                    }


                }
            });
            FirebaseInstanceId.getInstance().getInstanceId().addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    dialog.setMessage("Pairing Failed due to device token");
                    dialog.dismiss();
                    Helper.showErrorDialog(ScanQRCode.this, "Could not get device token..try later");
                }
            });


        }else {
            Helper.showErrorDialog(ScanQRCode.this, "Internet Not Available");
        }
    }
}
