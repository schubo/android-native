package schoolbus.ego.com.goschool.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class School implements Parcelable {
    private int id=-1;
    private String name;
    private String street;
    private String streetNumber;
    private int zipcode;
    private String town;
    private String phoneNumber;
    private String latitude;
    private String longitude;
    public School(){

    }

    public School(Parcel in) {
        id = in.readInt();
        name = in.readString();
        streetNumber = in.readString();
        zipcode = in.readInt();
        town = in.readString();
        phoneNumber = in.readString();
    }

    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public static Creator<School> getCREATOR() {
        return CREATOR;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(street);
        dest.writeString(streetNumber);
        dest.writeInt(zipcode);
        dest.writeString(town);
        dest.writeString(phoneNumber);

    }
}
