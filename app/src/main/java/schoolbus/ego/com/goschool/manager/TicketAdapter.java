package schoolbus.ego.com.goschool.manager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.model.Child;
import schoolbus.ego.com.goschool.model.Ticket;
import schoolbus.ego.com.goschool.utils.Helper;
import schoolbus.ego.com.goschool.view.BookingActivity;
import schoolbus.ego.com.goschool.view.ConfigureChildActivity;
import schoolbus.ego.com.goschool.view.RideDetailsActivity;

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.TicketViewHolder>{
    private List<Ticket> ticketItemList;
    private Context context;
private ProgressDialog dialog;


    public TicketAdapter(List<Ticket> ticketItemList, Context context) {
        this.ticketItemList = ticketItemList;
        this.context = context;
        dialog=new ProgressDialog(context);
    }
    @Override
    public TicketAdapter.TicketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View ticketView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_ticket, parent, false);
        TicketAdapter.TicketViewHolder vh = new TicketAdapter.TicketViewHolder(ticketView);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull TicketViewHolder ticketViewHolder, final int i) {
        if(ticketItemList.get(i).getStartAddress()!=null){
            ticketViewHolder.startAddress.setText(ticketItemList.get(i).getStartAddress().getAddressLine(0));
        }else {
            ticketViewHolder.startAddress.setText("No address found");
        }
        if(ticketItemList.get(i).getDestinationAddress()!=null){
            ticketViewHolder.endAddress.setText(ticketItemList.get(i).getDestinationAddress().getAddressLine(0));
        }else {
            ticketViewHolder.endAddress.setText("");
        }
        if(ticketItemList.get(i).getDate()!=null){

            ticketViewHolder.date.setText(String.valueOf(Helper.getBookingDate(context,ticketItemList.get(i).getDate())));
        }else {
            ticketViewHolder.date.setText("");
        }

        ticketViewHolder.bookingID.setText(String.valueOf(ticketItemList.get(i).getBookingId()));
        ticketViewHolder.name.setText(String.valueOf(ticketItemList.get(i).getName().substring(0, ticketItemList.get(i).getName().length() - 1)));
        ticketViewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Edit",Toast.LENGTH_SHORT).show();
            }
        });
        ticketViewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.setMessage("Deleting Child...");
                dialog.show();
                SharedPreferenceManager sMgr = new SharedPreferenceManager();
                if (sMgr.retrieveParentJWT(context) != null | sMgr.retrieveParentJWT(context).equals("")) {
                    APIcallManager apiMgr = new APIcallManager();
                    apiMgr.deleteBooking(context, sMgr.retrieveParentJWT(context), Integer.parseInt(ticketItemList.get(i).getBookingId()),apiStatus);
                } else {
                    dialog.setMessage("Could not be deleted");
                    dialog.dismiss();

                }
            }
        });
        ticketViewHolder.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(context,"Details",Toast.LENGTH_SHORT).show();

                Intent intent=new Intent(context,RideDetailsActivity.class);
                intent.putExtra(Ticket.TAG_StartLatitude,ticketItemList.get(i).getSourceLatitude());
                intent.putExtra(Ticket.TAG_StartLongitude,ticketItemList.get(i).getSourceLongitude());
                intent.putExtra(Ticket.TAG_DestLatitude,ticketItemList.get(i).getDestLatitude());
                intent.putExtra(Ticket.TAG_DestLongitude,ticketItemList.get(i).getDestLongitude());


                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return ticketItemList.size();
    }
    public class TicketViewHolder extends RecyclerView.ViewHolder {
        TextView startAddress;
        TextView endAddress;
        TextView date;
        TextView name;
        TextView bookingID;
        ImageButton edit;
        ImageButton delete;
        ImageButton details;


        public TicketViewHolder(View view) {
            super(view);
            startAddress=view.findViewById(R.id.tv_start_address);
            endAddress=view.findViewById(R.id.tv_end_address);
            date=view.findViewById(R.id.tripDate);
            name=view.findViewById(R.id.ticket_student_name);
            bookingID=view.findViewById(R.id.tv_booking_id);
            edit=view.findViewById(R.id.icon_edit);
            delete=view.findViewById(R.id.icon_delete);
            details=view.findViewById(R.id.btn_details);
        }
    }
    private APIcallManager.DeleteBookingApiStatus apiStatus=new APIcallManager.DeleteBookingApiStatus() {
        @Override
        public void onBookingDeleteSuccess() {
            ((BookingActivity)context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(dialog!=null) {
                        dialog.setMessage("Deleted Successfully");
                        dialog.dismiss();
                        if(dialog.isShowing())
                            dialog.dismiss();
                        ((BookingActivity)context).onBookingDeleteSuccess();
                    }
                }

            });
        }

        @Override
        public void onBookingDeleteFailure(final  String msg) {
            ((BookingActivity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (dialog != null) {
                        dialog.setMessage("Deletion Failed");
                        dialog.dismiss();
                        if (dialog.isShowing())
                            dialog.dismiss();
                    }
                    ((BookingActivity) context).onBookingDeleteFailure(msg);
                }


            });
        }
    };
}
