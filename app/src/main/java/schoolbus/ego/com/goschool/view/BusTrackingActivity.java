package schoolbus.ego.com.goschool.view;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.DrawableRes;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import schoolbus.ego.com.goschool.R;
import schoolbus.ego.com.goschool.manager.VolleySingleton;
import schoolbus.ego.com.goschool.model.DirectionObject;
import schoolbus.ego.com.goschool.model.GsonRequest;
import schoolbus.ego.com.goschool.model.LegsObject;
import schoolbus.ego.com.goschool.model.PolylineObject;
import schoolbus.ego.com.goschool.model.RouteObject;
import schoolbus.ego.com.goschool.model.StepsObject;
import schoolbus.ego.com.goschool.model.TrackBus;
import schoolbus.ego.com.goschool.utils.CustomException;
import schoolbus.ego.com.goschool.utils.DataProviderHelper;
import schoolbus.ego.com.goschool.utils.Helper;

public class BusTrackingActivity extends FragmentActivity implements OnMapReadyCallback {
    private  final String TAG = this.getClass().getSimpleName();
    private GoogleMap mMap;
    private Marker marker;
    private TextView startPoint;
    private TextView endPoint;
    private LinearLayout lay_trip_details;
    private List<LatLng> latLngList=new ArrayList<LatLng>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_tracking);
        startPoint=findViewById(R.id.tv_start_point_address);
        endPoint=findViewById(R.id.tv_end_point_address);
        lay_trip_details=findViewById(R.id.lay_trip_details);

        latLngList.add(new LatLng(50.78217,6.12728)); //source
        latLngList.add(new LatLng(50.7802,6.05945));  // destination

        latLngList.add(new LatLng(50.7708,6.11948));  //meeting point

        try {
            startPoint.setText(Helper.getAddress(this,50.78217,6.12728).getAddressLine(0));
            endPoint.setText(Helper.getAddress(this,50.7802,6.05945).getAddressLine(0));
        } catch (CustomException e) {
            e.printStackTrace();
            lay_trip_details.setVisibility(View.INVISIBLE);
        }

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        LatLng origin = latLngList.get(0);
        createMarker(origin,1);
        LatLng destination = latLngList.get(1);
        createMarker(destination,2);

        addBusMarker(origin);
        //use Google Direction API to get the route between these Locations
        String directionApiPath = Helper.getUrl(String.valueOf(origin.latitude), String.valueOf(origin.longitude),
                String.valueOf(destination.latitude), String.valueOf(destination.longitude));
        Log.d(TAG, "Path " + directionApiPath);
        getDirectionFromDirectionApiServer(directionApiPath);

        addCameraToMap(origin);


    }


    private void addBusMarker(LatLng loc){
        if(marker!=null){
            marker.remove();
        }

        MarkerOptions options=new MarkerOptions()
                .icon(bitmapDescriptorFromVector(this,R.drawable.ic_bus))
                .snippet(loc.latitude+","+loc.longitude)
                .position(loc);


        // Add a marker in Sydney and move the camera

        marker= mMap.addMarker(options);

        // Zoom map
        CameraUpdate center=CameraUpdateFactory.newLatLng(new LatLng(DataProviderHelper.getCurrentLocationOfBus().getLatitude(), DataProviderHelper.getCurrentLocationOfBus().getLongitude()));
        CameraUpdate zoom=CameraUpdateFactory.zoomTo(11);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    private void refreshMap(GoogleMap mapInstance){
        mapInstance.clear();
    }
    private void createMarker(LatLng latLng, int position){
        MarkerOptions mOptions = new MarkerOptions().position(latLng);
        if(position == 1){
            mOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            try {
                mOptions.title("Source: "+Helper.getAddress(this,latLng.latitude,latLng.longitude).getThoroughfare());
            } catch (CustomException e) {
                e.printStackTrace();
            }
        }else{
            mOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

            try {
                mOptions.title("Destination: "+Helper.getAddress(this,latLng.latitude,latLng.longitude).getThoroughfare());
            } catch (CustomException e) {
                e.printStackTrace();
            }
        }
        addCameraToMap(latLng);
        mMap.addMarker(mOptions);
    }
    private void addCameraToMap(LatLng latLng){
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(12)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
    private void animateMarker(GoogleMap myMap, final Marker marker, final List<LatLng> directionPoint,
                               final boolean hideMarker) throws Exception{
        try {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            Projection proj = myMap.getProjection();
            final long duration = 600000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                int i = 0;

                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed
                            / duration);
                    if(i<directionPoint.size()){
                        Location location = new Location(String.valueOf(directionPoint.get(i)));
                        int j=i; j++;
                        if(j<directionPoint.size()){
                            Location newlocation = new Location(String.valueOf(directionPoint.get(i + 1)));
                            marker.setAnchor(0.5f, 0.5f);
                            marker.setRotation(location.bearingTo(newlocation) - 45);
                            if (i < directionPoint.size()) {
                                marker.setPosition(directionPoint.get(i));
                            }

                            i++;
                        }


                    }




                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 100);
                    } else {
                        if (hideMarker) {
                            marker.setVisible(false);
                        } else {
                            marker.setVisible(true);
                        }
                    }
                }
            });
        }catch (Exception ex){
            throw new Exception();
        }
    }
    private void getDirectionFromDirectionApiServer(String url){
        GsonRequest<DirectionObject> serverRequest = new GsonRequest<DirectionObject>(
                Request.Method.GET,
                url,
                DirectionObject.class,
                createRequestSuccessListener(),
                createRequestErrorListener());
        serverRequest.setRetryPolicy(new DefaultRetryPolicy(
                Helper.MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(serverRequest);
    }
    private Response.Listener<DirectionObject> createRequestSuccessListener() {
        return new Response.Listener<DirectionObject>() {
            @Override
            public void onResponse(DirectionObject response) {
                try {
                    Log.d("JSON Response", response.toString());
                    if(response.getStatus().equals("OK")){
                        List<LatLng> mDirections = getDirectionPolylines(response.getRoutes());
                        drawRouteOnMap(mMap, mDirections);
                        // TODO: 07-01-2019
                        try {
                            animateMarker(mMap, marker, mDirections, false);
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }

                    }else{
                        Toast.makeText(BusTrackingActivity.this, R.string.server_error, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };
        };
    }


    private List<LatLng> getDirectionPolylines(List<RouteObject> routes){
        List<LatLng> directionList = new ArrayList<LatLng>();
        for(RouteObject route : routes){
            List<LegsObject> legs = route.getLegs();
            for(LegsObject leg : legs){
                // todo fix this
                // String routeDistance = leg.getDistance().getText();
                // String routeDuration = leg.getDuration().getText();

                String routeDistance="";
                String routeDuration ="";

                List<StepsObject> steps = leg.getSteps();
                for(StepsObject step : steps){
                    PolylineObject polyline = step.getPolyline();
                    String points = polyline.getPoints();
                    List<LatLng> singlePolyline = decodePoly(points);
                    for (LatLng direction : singlePolyline){
                        directionList.add(direction);
                    }
                }
            }
        }
        return directionList;
    }
    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        };
    }
    private void drawRouteOnMap(GoogleMap map, List<LatLng> positions){
        PolylineOptions options = new PolylineOptions().width(5).color(Color.RED).geodesic(true);
        options.addAll(positions);
        Polyline polyline = map.addPolyline(options);
    }
    /**
     * Method to decode polyline points
     * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     * */
    private List<LatLng> decodePoly(String encoded) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;
            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }
        return poly;
    }
}
