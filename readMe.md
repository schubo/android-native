**Documentation for Schubo app.**

Schubo application provides registered parents to manage their own account, book suitable ride for their child and live tracking of the ride.

**Android Environment setup:**

Schubo application developer installation is as follows

1. _Install the Java Development Kit:_
    ownload and install the [Java Development Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html). Unity requires the 64-bit version JDK 8 (1.8).

2. Download SDK using Android Studio: Install Android studio from the [Android developer portal](https://developer.android.com/index.html). The Android developer portal provides detailed installation instructions. 
   When installing the Android platform SDK and other tools, you can typically install the latest available version.
   
3. _Enable USB debugging on your device_
    To enable USB debugging, you must enable Developer options on your device. To do this, find the build number in your device&#39;s Settings menu. The location of the build number varies between devices. The Android setting can be found by navigating _to Settings \&gt; About phone \&gt; Build number._ Once you are navigated to build number, tap seven times on build number to unlock developer option. 
    Once your developer option is enabled, go to Settings-\&gt; Developer Options,then enable checkbox of USB debugging.

**Installing Source Tree and Setup Schubo application development**

1. _Download and install_ [_Source Tree_](https://www.sourcetreeapp.com/) .
   If you want to connect to a remote hosting service, use the default options as you enter your GitHub credentials.

2. _Cloning Schubo Application from repository_:
   To clone Schubo application repository, add GitHub account. Click the settings icon and add account. Click the Add from Account tab and connect to GitHub account. Keep default Auth Type.
   Clone repository repository to destination local repository by using URL:
   [https://gitlab.com/schubo/android-native.git](https://gitlab.com/schubo/android-native.git)

**Running Schubo Android application:**

1. Open/Add the local Schubo application repository into Android Studioand run the application module
2. _Run on Real Device_:

   * Connect your device to your development machine with a USB cable. You might need to [install the appropriate USB driver](https://developer.android.com/studio/run/oem-usb.html) for your device. 
   * In Android Studio, click the _Schubo_ app module in the Project window, and Run...
   * In the _Select Deployment Target_ window, select the device, and click _OK_.

   Android Studio installs the **Schubo** app in connected device and starts it.

   _Run on Emulator:_

   * In the _Select Deployment Target_ window, Click _Create Virtual Device._
   * In the Select Hardware screen, _choose a device definition._
   * In the _System Image_ screen, select the version with the highest API level. If you don&#39;t have that version installed, a _Download_ link is shown, so click that and complete the download.
   * On the _Android Virtual Device (AVD)_ screen, leave all the settings alone and click _Finish_.
   * In the _Select Deployment Target_ Window, select the device you just created and click _OK._

   Android Studio installs the **Schubo** app on Virtual Device and starts it.
