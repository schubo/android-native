package egoDigital.schubochild;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends Activity {

    public static String qrCodeResult = "QRCodeResult";

    public static String feedback = "noFeedback";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton booking = (ImageButton) findViewById(R.id.booking);
        //booking.setEnabled(false);

        ImageButton feedback = (ImageButton) findViewById(R.id.feedback);
        //feedback.setEnabled(false);

        ImageButton qrcode = (ImageButton) findViewById(R.id.qrcode);
        //qrcode.setEnabled(false);
    }

    public void pairing(View view){
        Intent pairingIntent = new Intent(MainActivity.this, ScanActivity.class);
        startActivity(pairingIntent);

        if (qrCodeResult != "QRCodeResult"){
            ImageButton booking = (ImageButton) findViewById(R.id.booking);
            booking.setEnabled(true);

            ImageButton feedback = (ImageButton) findViewById(R.id.feedback);
            feedback.setEnabled(true);

            ImageButton qrcode = (ImageButton) findViewById(R.id.qrcode);
            qrcode.setEnabled(true);
        }
    }

    public void showBooking(View view){
        Intent showBookingIntent = new Intent(MainActivity.this, BookingActivity.class);
        startActivity(showBookingIntent);
    }

    public void generateQRCode(View view){
        Intent generateQRCodeIntent = new Intent(MainActivity.this, GenerateQRCode.class);
        startActivity(generateQRCodeIntent);
    }

    public void giveFeedback(View view){
        Intent giveFeedbackIntent = new Intent(MainActivity.this, FeedbackActivity.class);
        startActivity(giveFeedbackIntent);
    }


    /*
    public void toastMe(View view){
        Toast myToast = Toast.makeText(this, "Hello Toast!", Toast.LENGTH_SHORT);
        myToast.show();
    }

    public void countMe(View view){
        TextView showCount = (TextView) findViewById(R.id.textView);
        String countString = showCount.getText().toString();
        Integer count = Integer.parseInt(countString);
        count++;

        showCount.setText(count.toString());
    }

    public void randomMe(View view){
        Intent randomIntent = new Intent(MainActivity.this, SecondActivity.class);
        TextView showCount = (TextView) findViewById(R.id.textView);
        String countString = showCount.getText().toString();
        int count = Integer.parseInt(countString);
        randomIntent.putExtra(TOTAL_COUNT, count);

        startActivity(randomIntent);
    }
    */
}
