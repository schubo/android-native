package egoDigital.schubochild;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.qrcode.QRCodeWriter;

public class GenerateQRCode extends AppCompatActivity {

    ImageView qrCodeImage;
    Button generateQRCode;
    String qrCodeContent = "test";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_qrcode);

        qrCodeImage = (ImageView) findViewById(R.id.QRCode);
        generateQRCode = (Button) findViewById(R.id.generateQRCode);

    }

    public void generateQRCode(View view){

        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix qrCodeMatrix = writer.encode(qrCodeContent, BarcodeFormat.QR_CODE, 150, 150);
            int width = qrCodeMatrix.getWidth();
            int height = qrCodeMatrix.getHeight();
            Bitmap qrCodeBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

            for (int i = 0; i < width; i++){
                for (int j = 0; j < height; j++){
                    qrCodeBitmap.setPixel(i, j, qrCodeMatrix.get(i, j) ? Color.BLACK: Color.WHITE);
                }
            }
            qrCodeImage.setImageBitmap(qrCodeBitmap);
        }catch (WriterException e){
            e.printStackTrace();
        }
    }
}
