package egoDigital.schubochild;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class FeedbackActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
    }

    public void feedback(View view){
        Toast toast = Toast.makeText(this, "Feedback send", Toast.LENGTH_SHORT);
        ImageView image = new ImageView(this);
        image.setImageResource(R.drawable.success);
        toast.setView(image);
        toast.show();

        finish();
    }
}
